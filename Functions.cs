﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2018 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using System.Diagnostics;
using System.Text.RegularExpressions;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Goblinsama
{
	public static class Functions
	{
		/// <summary>
		/// Converte una stringa (ASCII) in byte[]
		/// </summary>
		/// <param name="str">Stringa di origine</param>
		/// <param name="bytes">Byte di destinazione</param>
		/// <param name="skip">Quantità di byte da saltare sulla destinazione prima di scriverci</param>
		/// <param name="length">Lunghezza massima della stringa</param>
		public static void GetBytes ( string str, byte[] bytes, int skip=0, int length=0 )
		{
			if (length==0)
				length=str.Length;
			else if (length<0)
				throw new UnityException("length<0 not supported");
			if (skip<0)
				throw new UnityException("skip<0 not supported");
			
			int ii=0;
			while (true)
			{
				if (skip>=bytes.Length)
					break;
				if (ii>=length)
					break;
				
				bytes[skip++]=(byte)str[ii++];
			}
		}
		/// <summary>
		/// Converte un byte[] in stringa (ASCII)
		/// </summary>
		/// <returns>La stringa di destinazione</returns>
		/// <param name="bytes">I byte di origine</param>
		/// <param name="skip">Quantità di byte da saltare dall'origine prima di leggere</param>
		/// <param name="length">Massima lunghezza della stringa</param>
		public static string GetString ( byte[] bytes, int skip=0, int length=0 )
		{
			if (length==0)
				length=bytes.Length;
			
			string ret="";
			int ii=0; // NOTA: se ne potrebbe fare a meno, ma così è più leggibile
			while (true)
			{
				if (skip>=bytes.Length)
					break;
				if (ii>=length)
					break;
				
				ret=ret+(char)bytes[skip++];
				ii++;
			}
			
			return ret;
		}
		
		/// <summary>
		/// Inserisce una stringa in un byte[] (con tanto di byte di lunghezza)
		/// </summary>
		/// <param name="str">La stringa di origine</param>
		/// <param name="bytes">I byte di destinazione</param>
		/// <param name="cur">L'indice a cui scrivere nella destinazione</param>
		public static void InsertString ( string str, byte[] bytes, ref int cur )
		{
			bytes[cur++]=(byte)str.Length;
			GetBytes(str,bytes,cur);
			cur+=str.Length;
		}
		/// <summary>
		/// Legge una stringa da un byte[] (usando il byte di lunghezza)
		/// </summary>
		/// <returns>La stringa di destinazione</returns>
		/// <param name="bytes">I byte di origine</param>
		/// <param name="cur">L'indice a cui leggere dalla destinazione</param>
		public static string RetrieveString ( byte[] bytes, ref int cur )
		{
			int len=bytes[cur++];
			string ret=GetString(bytes,cur,len);
			cur+=len;
			return ret;
		}
		
		public static void InsertInt16 ( int src, byte[] dest, ref int cur )
		{
			byte[] ba=System.BitConverter.GetBytes((short)src);
			dest[cur++]=ba[0];
			dest[cur++]=ba[1];
		}
		public static int RetrieveInt16 ( byte[] src, ref int cur )
		{
			int ret=System.BitConverter.ToInt16(src,cur);
			cur+=2;
			return ret;
		}
		public static void InsertByte ( byte src, byte[] dest, ref int cur )
		{
			byte[] ba=System.BitConverter.GetBytes((byte)src);
			dest[cur++]=ba[0];
		}
		public static void InsertByte ( int src, byte[] dest, ref int cur )
		{
			InsertByte((byte)src,dest,ref cur);
		}
		public static byte RetrieveByte ( byte[] src, ref int cur )
		{
			byte ret=(byte)System.BitConverter.ToChar(src,cur);
			cur+=1;
			return ret;
		}
		
		public static bool ParseInt32 ( string src, ref int dest )
		{
			int val;
			if ( System.Int32.TryParse(src,out val) )
			{
				dest = val;
				return true;
			}
			return false;
		}
		
		public static void InsertBytes ( byte[] src, byte[] dest, int cr, ref int cw, byte length )
		{
			dest[cw++]=length;
			for (int ii=0; ii<length; ii++)
			{
				dest[cw++]=src[cr++];
			}
		}
		public static byte[] RetrieveBytes ( byte[] bytes, ref int cur )
		{
			byte[] ret;
			byte len = bytes[cur++];
			ret = new byte[len];
			
			for (int ii=0; ii<len; ii++)
			{
				ret[ii]=bytes[cur++];
			}
			
			return ret;
		}
		
		// http://forum.unity3d.com/threads/8798-custom-Command-Line-arguments?p=309164&viewfull=1#post309164
		// amirebrahimi (San Francisco, CA) ::Amir
		public static string[] GetCommandLineArgs ()
		{
			#if UNITY_STANDALONE_OSX || UNITY_STANDALONE_LINUX
			Process proc = Process.GetCurrentProcess();
			ProcessStartInfo si = new ProcessStartInfo("ps", "-p" + proc.Id + " -xwwocommand=");
			si.RedirectStandardOutput = true;
			si.RedirectStandardError = true;
			si.UseShellExecute = false;
			si.CreateNoWindow = true;
			
			Process psProc = new Process();
			psProc.StartInfo = si;
			psProc.Start();
			string result = psProc.StandardOutput.ReadToEnd();
			return result.Split();
			#else
			throw new System.NotImplementedException("GetCommandLineArgs unsupported on <"+Application.platform+">");
			#endif
		}
		
		public static string MangleVersion (string source)
		{
			if (source==null)
				return null;
			
			string ret = source;
			// TODO. non ha molto senso che prima converta in ';' e poi elimini TUTTI i ';': dovrebbe solo eliminare quelli doppi, se ci sono?
			ret = Regex.Replace(ret, "[^0-9.]+([0-9]+)", ";$1"); // sostituisce i blocchi di lettere con dei ';', se sono seguiti da numeri
			ret = Regex.Replace(ret, "[^0-9.;]+", ""); // elimina i blocchi di lettere rimasti
			
			if (ret.Length>16)
				// TODO. invece controllare effettivamente che sia corretto
				return null;
			
			return ret;
		}
		
		public static string CleanBr (string source)
		{
			// patter regex dei caratteri indesiderati
			string rgx = "[\\r\\n]+";
			
			// "pulisce" le stringhe utente e password da caratteri non voluti
			return Regex.Replace(source, rgx, "");
		}
		public static void CleanBrInputField (InputField target)
		{
			target.text = CleanBr(target.text);
		}
		
		public static char[] trimChars = {' ','\n','\r','\t'};
		public static void TrimEndInputField (InputField target)
		{
			target.text = target.text.TrimEnd(trimChars);
		}
		public static void TrimStartInputField (InputField target)
		{
			target.text = target.text.TrimStart(trimChars);
		}
		public static void TrimInputField (InputField target)
		{
			target.text = target.text.Trim(trimChars);
		}
		
		public static void TrimX (ref string target)
		{
			target = target.Trim(trimChars);
		}
		
		public static void BroadcastAll (string methodName, object parameter, SendMessageOptions options=SendMessageOptions.DontRequireReceiver)
		{
			if (options == SendMessageOptions.RequireReceiver)
			{
				UnityEngine.Debug.LogWarning("Warning: using Goblinsama.BroatcastAll with RequireReceiver will currently likely give you unexpected results.");
				/* NOTA: il problema è che il comportamento non è consistente, in quanto richiede che OGNI oggetto toplevel gestisse il messaggio, mentre
				 * il comportamento che ci si aspetta è che TRA TUTTI ci sia almeno uno che lo gestisca.
				*/
			}
			
			foreach (GameObject go in GameObject.FindObjectsOfType<GameObject>())
			{
				if (go.transform.parent!=null)
				{
					// non è un toplevel
					continue;
				}
				
				go.BroadcastMessage(methodName,parameter,options);
			}
		}
		
		public static KeyCode IntToAlpha (int num)
		{
			switch (num)
			{
			case 1: return KeyCode.Alpha1;
			case 2: return KeyCode.Alpha2;
			case 3: return KeyCode.Alpha3;
			case 4: return KeyCode.Alpha4;
			case 5: return KeyCode.Alpha5;
			case 6: return KeyCode.Alpha6;
			case 7: return KeyCode.Alpha7;
			case 8: return KeyCode.Alpha8;
			case 9: return KeyCode.Alpha9;
			case 0: return KeyCode.Alpha0;
			default: return KeyCode.None;
			}
		}
		public static KeyCode IntToNumpad (int num)
		{
			switch (num)
			{
			case 1: return KeyCode.Keypad1;
			case 2: return KeyCode.Keypad2;
			case 3: return KeyCode.Keypad3;
			case 4: return KeyCode.Keypad4;
			case 5: return KeyCode.Keypad5;
			case 6: return KeyCode.Keypad6;
			case 7: return KeyCode.Keypad7;
			case 8: return KeyCode.Keypad8;
			case 9: return KeyCode.Keypad9;
			case 0: return KeyCode.Keypad0;
			default: return KeyCode.None;
			}
		}
		
		public static string StringListToString ( string[] list )
		{
			string ret="";
			foreach (string line in list)
			{
				if (ret.Length > 0)
					ret += "\n";
				ret += line;
			}
			return ret;
		}
		public static string InterlaceString ( string source, string interlace, int length )
		{
			string ret="";
			for (int cur=0; cur<source.Length; cur+=length)
			{
				if (ret!="")
					ret += interlace;
				
				ret += source.Substring(cur,length);
			}
			return ret;
		}
		
		/// <summary>
		/// Ritorna ciò che spera sia un nome o dell'utente o quantomeno della macchina.
		/// Potrebbe funzionare come no.
		/// </summary>
		public static string GetUserName ()
		{
			#if UNITY_STANDALONE
			return System.Environment.UserName;
			#else
			return SystemInfo.deviceName;
			#endif
		}
		
		public static void Exit ()
		{
			#if UNITY_EDITOR
			if (Application.isEditor)
			{
				UnityEditor.EditorApplication.isPlaying = false;
			}
			else
			#endif
			{
				Application.Quit();
			}
		}
		
		public static Vector2 NormalizeVector ( Vector2 vec )
		{
			Vector2 ret;
			ret.x = vec.x > 0 ? 1 : ( vec.x < 0 ? -1 : 0 );
			ret.y = vec.y > 0 ? 1 : ( vec.y < 0 ? -1 : 0 );
			return ret;
		}
		public static Vector3 RoundVector ( Vector3 vec )
		{
			return new Vector3( Mathf.Round( vec.x ), Mathf.Round( vec.y ), Mathf.Round( vec.z ) );
		}
		public static float RoundToNearest ( float val, float nearest )
		{
			return Mathf.Round( val / nearest ) * nearest;
		}
		
		public static string WeekDayToString ( int weekDayInt )
		{
			return System.Globalization.CultureInfo.InvariantCulture.DateTimeFormat.GetDayName( (System.DayOfWeek)WeekDay7to0( weekDayInt ) );
		}
		public static int WeekDay7to0 ( int weekDayInt )
		{
			if ( weekDayInt == 7 )
				return 0;
			return weekDayInt;
		}
		public static int WeekDay0to7 ( int weekDayInt )
		{
			if ( weekDayInt == 0 )
				return 7;
			return weekDayInt;
		}
		
		public static void UpdateObject ( GameObject obj, string title )
		{
			UpdateEditor( obj, () => {
				obj.name = title;
			}, "Fixing " + title );
		}
		public static void UpdateEditor ( Object obj, System.Action act, string title = null )
		{
			title = title == null ? "UpdateEditor" : title;
			
			#if UNITY_EDITOR
			
			UnityEditor.Undo.RecordObject( obj, title );
			
			#endif
			
			act();
		}
		
		public static float GetAxis ( string axisName )
		{
			if ( axisName.Length < 1 )
				return 0;
			return Input.GetAxis( axisName );
		}
		public static bool GetJoyButton ( int joyNumber, int buttonNumber )
		{
			return GetJoyButtonDownX( Input.GetKey, joyNumber, buttonNumber );
		}
		public static bool GetJoyButtonDown ( int joyNumber, int buttonNumber )
		{
			return GetJoyButtonDownX( Input.GetKeyDown, joyNumber, buttonNumber );
		}
		private static bool GetJoyButtonDownX ( System.Func<string,bool> act, int joyNumber, int buttonNumber )
		{
			if ( joyNumber < 0 )
				return false;
			if ( buttonNumber < 0 )
				return false;
			
			return act( "joystick" + ( ( joyNumber == 0 ) ? "" : " " + joyNumber ) + " button " + buttonNumber );
		}
		
		public static void FillCollectionV3 ( ICollection<Vector3> list, GameObject parent )
		{
			foreach ( Transform tr in parent.transform )
			{
				list.Add( tr.position );
			}
		}
		
		public static Texture2D CreateTexture2D ( int sizeX, int sizeY, Color background )
		{
			Vector2 texSize = new Vector2( sizeX, sizeY );
			Texture2D tex = new Texture2D( (int)texSize.x, (int)texSize.y );
			
			for ( int ix = 0; ix < sizeX; ix++ )
			{
				for ( int iy = 0; iy < sizeY; iy++ )
				{
					tex.SetPixel( ix, iy, Color.black );
				}
			}
			
			tex.Apply();
			
			return tex;
		}
		public static Sprite CreateSprite ( int sizeX, int sizeY, Color background )
		{
			Texture2D tex = CreateTexture2D( sizeX, sizeY, background );
			Sprite sprite = Sprite.Create( tex, new Rect( Vector2.zero, new Vector2( sizeX, sizeY ) ), Vector2.zero );
			return sprite;
		}
		
		public static string GetConfigDir (string GAME_NAME)
		{
			#if UNITY_STANDALONE_OSX
			return System.Environment.GetEnvironmentVariable("HOME")+"/Library/Application Support/"+GAME_NAME+"/";
			#elif UNITY_STANDALONE_WIN
			return System.Environment.SpecialFolder.LocalApplicationData+"/";
			#elif UNITY_STANDALONE_LINUX
			return System.Environment.GetEnvironmentVariable("HOME")+"/."+GAME_NAME+"/";
			#else
			throw new System.NotImplementedException("GetConfigDir unsupported on <"+Application.platform+">");
			#endif
		}
	}
}
