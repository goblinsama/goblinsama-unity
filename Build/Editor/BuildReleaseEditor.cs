﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

#if UNITY_EDITOR

using UnityEngine;
using System.Collections.Generic;
using System.IO;
using UnityEditor;

namespace Goblinsama.Build
{
	public class BuildReleaseEditor : MonoBehaviour
	{
		[UnityEditor.MenuItem("Goblinsama/Build/Build Debug")]
		public static void CallBuildDebug ()
		{
			FindObjectOfType<BuildRelease>().DoBuildRelease(false,false,false,true);
		}
		[UnityEditor.MenuItem("Goblinsama/Build/Build All")]
		public static void CallBuildAll ()
		{
			FindObjectOfType<BuildRelease>().DoBuildRelease(true,true,true,false);
		}
		[UnityEditor.MenuItem("Goblinsama/Build/Build Server")]
		public static void CallBuildServer ()
		{
			FindObjectOfType<BuildRelease>().DoBuildRelease(true,false,false,false);
		}
		[UnityEditor.MenuItem("Goblinsama/Build/Build Standalone")]
		public static void CallBuildStandalone ()
		{
			FindObjectOfType<BuildRelease>().DoBuildRelease(false,true,false,false);
		}
		[UnityEditor.MenuItem("Goblinsama/Build/Build Mobile")]
		public static void CallBuildMobile ()
		{
			FindObjectOfType<BuildRelease>().DoBuildRelease(false,false,true,false);
		}
		[UnityEditor.MenuItem("Goblinsama/Build/Build All Clients")]
		public static void CallBuildClients ()
		{
			FindObjectOfType<BuildRelease>().DoBuildRelease(false,true,true,false);
		}
		
		[UnityEditor.MenuItem("Goblinsama/Build/Show server plugins")]
		public static void CallShowServerPlugins ()
		{
			FindObjectOfType<BuildRelease>().ShowServerPlugins(true);
		}
		[UnityEditor.MenuItem("Goblinsama/Build/Hide server plugins")]
		public static void CallHideServerPlugins ()
		{
			FindObjectOfType<BuildRelease>().ShowServerPlugins(false);
		}
	}
}

#endif
