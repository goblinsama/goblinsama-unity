﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016-2018 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.IO;

#if UNITY_EDITOR

using System.Collections.Generic;
using UnityEditor;
using UnityEditor.Build.Reporting;

#else

#pragma warning disable 0414

#endif

namespace Goblinsama.Build
{
	public class BuildRelease : MonoBehaviour
	{
		private const string basePrefix = "Assets/";
		
		[SerializeField] private string scenePrefix = "scene/";
		[SerializeField] private string[] scenes;
		[SerializeField] private string[] serverScenes;
		[SerializeField] private string[] serverOnlyPlugins;
		[SerializeField] private string[] clientOnlyPlugins;
		
		#if UNITY_EDITOR
		
		public void DoBuildRelease ( bool buildServer, bool buildStandalone, bool buildMobile, bool buildDebug )
		{
			Debug.Log( "Building…" );
			System.DateTime timeStart = System.DateTime.UtcNow;
			
			string[] fsClient = new string[scenes.Length];
			string[] fsServer = new string[scenes.Length + serverScenes.Length];
			
			int ccur = 0, scur = 0;
			foreach ( string sc in scenes )
			{
				string fs = FullScene( sc );
				fsClient[ccur++] = fs;
				fsServer[scur++] = fs;
			}
			foreach ( string sc in serverScenes )
			{
				string fs = FullScene( sc );
				fsServer[scur++] = fs;
			}
			
			string buildDir = "Build";
			string baseDir = buildDir + "/Release/" + PlayerSettings.bundleVersion + "/";
			string midDir = "/zipdir";
			string exName = PlayerSettings.productName;
			
			BuildTarget originalTarget = EditorUserBuildSettings.activeBuildTarget;
			
			BuildOptions boBase = BuildOptions.None;
			BuildOptions boServer = BuildOptions.EnableHeadlessMode;
			BuildOptions boDebug = BuildOptions.Development | BuildOptions.AllowDebugging;
			
			// Debug
			
			if ( buildDebug )
			{
				ShowServerPlugins( true );
				BuildWrap( fsServer, buildDir, exName + ".app", BuildTarget.StandaloneOSX, boDebug );
			}
			
			// Server
			
			if ( buildServer )
			{
				ShowServerPlugins( true );
				BuildWrap( fsServer, baseDir + "Server" + midDir, exName + ".x86_64", BuildTarget.StandaloneLinux64, boServer );
			}
			
			// Standalone
			
			if ( buildStandalone )
			{
				ShowServerPlugins( false );
				BuildWrap( fsClient, baseDir + "Linux" + midDir,     exName + ".x86_64", BuildTarget.StandaloneLinux64,   boBase );
				BuildWrap( fsClient, baseDir + "OSX" + midDir,       exName + ".app",    BuildTarget.StandaloneOSX,       boBase );
				BuildWrap( fsClient, baseDir + "Windows" + midDir,   exName + ".exe",    BuildTarget.StandaloneWindows64, boBase );
				BuildWrap( fsClient, baseDir + "Windows32" + midDir, exName + ".exe",    BuildTarget.StandaloneWindows,   boBase );
			}
			
			// Mobile
			
			if ( buildMobile )
			{
				ShowServerPlugins( false );
				BuildWrap( fsClient, baseDir + "Android" + midDir, exName + ".apk", BuildTarget.Android, boBase );
			}
			
			// 
			
			#pragma warning disable 0618
			/* NOTA: lo considera obsoleto, e la funzione sostitutiva ha un parametro in più.
			 * Tuttavia, non è fornito un modo per ottenere questo parametro.
			 * Per cui non è possibile passare alla funzione nuova.
			 */
			EditorUserBuildSettings.SwitchActiveBuildTarget( originalTarget );
			#pragma warning restore 0618
			
			ShowServerPlugins( true );
			
			System.DateTime timeEnd = System.DateTime.UtcNow;
			System.TimeSpan timeElapsed = timeEnd - timeStart;
			
			Debug.Log( string.Format( "Built in {0}.", timeElapsed ) );
		}
		
		private void BuildWrap ( string[] levels, string dir, string fileName, BuildTarget target, BuildOptions options )
		{
			Directory.CreateDirectory( dir );
			
			string locationPathName = dir + "/" + fileName;
			BuildReport brep = BuildPipeline.BuildPlayer( levels, locationPathName, target, options );
			if ( brep.summary.totalErrors > 0 )
			{
				Debug.LogError( "Error building " + fileName + ", " + brep.summary.totalErrors + " errors" );
			}
			else
			{
				Debug.Log( locationPathName + " built successfully." );
			}
		}
		
		public void ShowServerPlugins ( bool show )
		{
			bool showServer = show;
			bool showClient = !show;
			
			ShowServerPlugins(serverOnlyPlugins,showServer);
			ShowServerPlugins(clientOnlyPlugins,showClient);
			
			AssetDatabase.Refresh();
		}
		private void ShowServerPlugins ( string[] pluginList, bool show )
		{
			foreach (string enam in pluginList)
			{
				int isl = enam.LastIndexOf('/');
				string path = basePrefix+enam.Substring(0,isl+1);
				string fnam = enam.Substring(isl+1);
				
				string hidden = path+"."+fnam;
				string shown = path+fnam;
				
				string from = show ? hidden : shown;
				string to = show ? shown : hidden;
				
				if (FileExists(to) && FileExists(from))
				{
					throw new System.ApplicationException("Both origin and destination files exist: ["+from+"] to ["+to+"].");
				}
				if (FileExists(to))
				{
					Debug.Log("Destination file ["+to+"] already exists.");
					continue;
				}
				if (!FileExists(from))
				{
					throw new System.ApplicationException("Origin file ["+from+"] doesn't exist.");
				}
				
				Debug.Log("Moving ["+from+"] to ["+to+"].");
				
				FileMove(from,to);
			}
		}
		
		private string FullScene (string st)
		{
			return basePrefix+scenePrefix+st+".unity";
		}
		
		#endif
		
		public static bool FileExists (string fullname)
		{
			return File.Exists(fullname)||Directory.Exists(fullname);
		}
		public static void FileMove (string from, string to)
		{
			if (File.Exists(from))
			{
				File.Move(from,to);
			}
			else if (Directory.Exists(from))
			{
				Directory.Move(from,to);
			}
			else
			{
				throw new IOException("Couldn't find source file ["+from+"].");
			}
			
			if (FileExists(from+".meta"))
				FileMove(from+".meta",to+".meta");
		}
	}
}
