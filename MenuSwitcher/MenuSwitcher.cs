﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections.Generic;

namespace Goblinsama.UI
{
	/// <summary> Gestisce le interazioni tra i menù, nella stessa scena. </summary>
	public class MenuSwitcher : MonoBehaviourE
	{
		[SerializeField] protected RectTransform showAtStart;
		[SerializeField] protected FeedbackPopup popupSuccess;
		[SerializeField] protected FeedbackPopup popupFailure;
		[SerializeField] protected List<GameObject> refreshOnShow;
		[SerializeField] protected YesOrNo _yesOrNo;
		
		public System.Action showOnCancel;
		public System.Action showOnMenu;
		
		private bool initialized = false;
		
		public YesOrNo yesOrNo {
			get {
				return _yesOrNo;
			}
		}
		
		protected override void Start ()
		{
			base.Start();
			
			if ( !initialized )
			{
				if ( showAtStart )
				{
					Show( showAtStart );
				}
				else
				{
					HideAll();
				}
			}
		}
		protected override bool Update ()
		{
			if ( base.Update() )
				return true;
			
			if ( showOnCancel != null && Input.GetButtonDown( "Cancel" ) )
			{
				showOnCancel();
				return true;
			}
			if ( showOnMenu != null && ( Input.GetKeyDown( KeyCode.Menu ) || Input.GetButtonDown( "Menu" ) ) )
			{
				showOnMenu();
				return true;
			}
			
			return false;
		}
		
		/// <summary> Abilita il menù dato, disabilitando tutti gli altri. </summary>
		/// <param name="target"> Il menù da abilitare. </param>
		public void Show ( RectTransform target )
		{
			Check( target );
			HideAll();
			//Debug.Log( this + " showing " + target );
			
			target.gameObject.SetActive( true );
			foreach ( GameObject go in refreshOnShow )
			{
				go.SendMessage( "Refresh", SendMessageOptions.DontRequireReceiver );
			}
		}
		
		/// <summary>
		/// Nasconde il menù specificato.
		/// </summary>
		public void Hide ( RectTransform target )
		{
			//Debug.Log( this + " hiding " + target );
			
			if ( !IsMyChild( target ) )
				return;
			
			target.gameObject.SetActive( false );
		}
		
		/// <summary> Abilita il menù dato, senza disabilitare quello sottostante. </summary>
		/// <param name="target"> Il menù da abilitare. </param>
		public void PopUp ( RectTransform target )
		{
			Check( target );
			target.gameObject.SetActive( true );
		}
		
		/// <summary> Disabilita tutti i menù. </summary>
		public void HideAll ()
		{
			//Debug.Log( this + " hiding all" );
			initialized = true;
			
			foreach ( RectTransform child in transform )
			{
				if ( child.GetComponent<DontHide>() )
					continue;
				
				child.gameObject.SetActive( false );
			}
		}
		
		public void BackToStart ()
		{
			Show( showAtStart );
		}
		
		protected void Check ( RectTransform target )
		{
			if ( target == null )
			{
				Debug.LogWarning( this + " trying to show null." );
				return;
			}
			
			if ( !IsMyChild( target ) )
			{
				throw new System.ArgumentException( "MenuSwitcher called on non-child [" + target + "]." );
			}
		}
		
		/// <summary> Controlla se un menù è figlio del canvas. </summary>
		/// <param name="target"> Il menù da controllare. </param>
		/// <returns> true se è figlio, false altrimenti. </returns>
		protected bool IsMyChild ( RectTransform target )
		{
			try
			{
				foreach ( RectTransform child in transform )
				{
					if ( child == target )
						return true;
				}
			}
			catch ( MissingReferenceException )
			{
			}
			
			return false;
		}
		
		public void PopupSuccess ( string text, System.Action callback=null )
		{
			PopupX( popupSuccess, text, callback );
		}
		public void PopupFailure ( string text, System.Action callback=null )
		{
			PopupX( popupFailure, text, callback );
		}
		private void PopupX ( FeedbackPopup target, string text, System.Action callback )
		{
			target.SetText( text );
			if ( callback != null )
			{
				target.SetCallback( callback );
			}
			PopUp( target.rectTransform );
		}
	}
}
