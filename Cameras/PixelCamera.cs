﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2010,2013-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using Ether;

namespace Goblinsama.Cameras
{
	public class PixelCamera : MonoBehaviourE
	{
		[SerializeField] private int spriteSize;
		[SerializeField] private float smoothScrolling;
		[SerializeField] private float minDistance;
		[SerializeField] private Camera nullCamera;
		
		/// <summary>
		/// Quanto deve zoomare all'altezza indicata.
		/// </summary>
		[SerializeField] private float[] zoomForH;
		
		/// <summary>
		/// L'altezza di riferimento per lo zoom.
		/// </summary>
		[SerializeField] private int[] targetH;
		
		/// <summary>
		/// Zoom: a che punto arrotondare per eccesso o per difetto.
		/// </summary>
		[SerializeField][Range(0,1)] private float roundingBalance;
		
		/// <summary>
		/// I targetH reali calcolati in base all'arrotondamento.
		/// </summary>
		private int[] actualTargetH;
		
		public Camera cameraSelf { get; private set; }
		[System.NonSerialized] public CameraEnd[] cameraBlockers = new CameraEnd[4];
		
		public Transform target {
			get {
				return targetTransform;
			}
			set {
				SetTarget(value,true);
			}
		}
		
		protected Transform targetTransform;
		
		private int screenH=-1;
		private Vector3 assign = new Vector3();
		private int zoomLevel;
		private Vector2 min,max,pos; // pixel
		
		/// <summary>
		/// Immediately snaps to target, instead of lerping.
		/// </summary>
		protected bool snap;
		
		// recycle
		
		private Vector3 v3;
		
		protected override void Awake ()
		{
			base.Awake();
			
			/// Componenti
			
			cameraSelf = GetComponent<Camera>();
			targetTransform = null;
			
			/// Zoom
			
			actualTargetH = new int[targetH.Length];
			int prevTH=0;
			for (int ii=0; ii<targetH.Length-1; ii++)
			{
				int diff = targetH[ii+1]-prevTH;
				int balancedDiff = Mathf.RoundToInt(diff*roundingBalance);
				actualTargetH[ii] = prevTH+balancedDiff;
				prevTH = targetH[ii+1];
			}
		}
		protected override void OnEnable ()
		{
			base.OnEnable();
			
			cameraBlockers[(int)CameraEnd.Side.left]   = null;
			cameraBlockers[(int)CameraEnd.Side.right]  = null;
			cameraBlockers[(int)CameraEnd.Side.bottom] = null;
			cameraBlockers[(int)CameraEnd.Side.top]    = null;
		}
		protected override bool LateUpdate ()
		{
			if (base.LateUpdate())
				return true;
			
			if (Screen.height!=screenH)
			{
				Recalc();
				Goblinsama.Functions.BroadcastAll( "UpdatedResolution", new Vector2(Screen.width,Screen.height), SendMessageOptions.DontRequireReceiver );
			}
			
			if (targetTransform==null)
				return true;
			
			assign = targetTransform.position;
			assign.z = transform.position.z;
			
			/*
			float distance = Vector3.Distance(assign,transform.position)*spriteSize;
			if (distance < minDistance)
			{
				transform.position = assign;
			}
			*/
			CheckBlockers(ref assign);
			
			if (snap)
			{
				Debug.Log("snap from " + transform.position + " to " + assign);
				transform.position = assign;
				snap = false;
			}
			else
			{
				v3 = Vector3.Lerp( transform.position, assign, smoothScrolling*Time.deltaTime );
				v3 = FunctionsExt.RoundToNearestPixel(v3, spriteSize);
				v3.z = assign.z;
				transform.position = v3;
			}
			
			return false;
		}
		
		public void Disable ()
		{
			cameraSelf.enabled=false;
			nullCamera.enabled=true;
		}
		public void ZoomMore ()
		{
			if (++zoomLevel>=zoomForH.Length)
				zoomLevel=zoomForH.Length-1;
			UpdateCameraSize();
		}
		public void ZoomLess ()
		{
			if (--zoomLevel<0)
				zoomLevel=0;
			UpdateCameraSize();
		}
		public virtual void SetTarget (Transform tgt, bool doSnap)
		{
			targetTransform = tgt;
			snap = doSnap;
		}
		
		private void CheckBlockers (ref Vector3 assign)
		{
			v3 = assign;
			
			if (cameraBlockers[(int)CameraEnd.Side.left])
			{
				if (v3.x<cameraBlockers[(int)CameraEnd.Side.left].blockPosition)
				{
					v3.x=cameraBlockers[(int)CameraEnd.Side.left].blockPosition;
				}
			}
			if (cameraBlockers[(int)CameraEnd.Side.right])
			{
				if (v3.x>cameraBlockers[(int)CameraEnd.Side.right].blockPosition)
				{
					v3.x=cameraBlockers[(int)CameraEnd.Side.right].blockPosition;
				}
			}
			
			if (cameraBlockers[(int)CameraEnd.Side.bottom])
			{
				if (v3.y<cameraBlockers[(int)CameraEnd.Side.bottom].blockPosition)
				{
					v3.y=cameraBlockers[(int)CameraEnd.Side.bottom].blockPosition;
				}
			}
			if (cameraBlockers[(int)CameraEnd.Side.top])
			{
				if (v3.y>cameraBlockers[(int)CameraEnd.Side.top].blockPosition)
				{
					v3.y=cameraBlockers[(int)CameraEnd.Side.top].blockPosition;
				}
			}
			
			if (Vector3.Distance(v3,assign) > minDistance)
			{
				assign = v3;
			}
		}
		private void Recalc ()
		{
			screenH = Screen.height;
			
			/// calcola lo zoom
			
			for (int ii=0; ii<actualTargetH.Length; ii++)
			{
				zoomLevel=ii;
				if (screenH<=actualTargetH[zoomLevel])
					break;
			}
			
			UpdateCameraSize();
		}
		private void UpdateCameraSize ()
		{
			snap = true;
			cameraSelf.orthographicSize = screenH / (float)spriteSize / zoomForH[zoomLevel];
			
			/* TODO-. trasformare in hook
			EZParallax ez = FindObjectOfType<EZParallax>();
			if (ez)
				ez.InitializeParallax();
			*/
			
			foreach (CameraEnd ce in FindObjectsOfType<CameraEnd>())
			{
				ce.SendMessage("Refresh");
			}
		}
	}
}
