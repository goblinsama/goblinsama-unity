﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using Goblinsama;

namespace Goblinsama.Cameras
{
	public class FollowTarget : MonoBehaviourE
	{
		public enum RotationBehaviour
		{
			Fixed,
			Follow,
			Ignore,
			LookAt, // ibrido tra Follow e Fixed
		}
		
		[SerializeField] private RotationBehaviour rotationBehaviour;
		[SerializeField] private Transform startingTarget;
		[SerializeField] private string[] avoidLayers;
		[SerializeField] private Vector3 avoidWallsPosition;
		
		/// <summary>
		/// For lerping, instead of snapping, when it switches to the avoidWalls position.
		/// </summary>
		[SerializeField] private float smoothing = 1;
		
		/// <summary>
		/// When have we gone back enough and we can stop smoothing.
		/// </summary>
		[SerializeField] private float goingBackSnapping = 0.1f;
		
		/// <summary>
		/// Imposta automaticamente la positionDiff all'avvio.
		/// </summary>
		[SerializeField] private bool autoSetPositionDiff;
		
		/// <summary>
		/// Usa l'asse verticale del mouse per la rotazione verticale (ovvero rotazione sull'asse X)
		/// </summary>
		[SerializeField] private bool mouseMoveV;
		
		/// <summary>
		/// Velocità spostamento Camera.
		/// </summary>
		[SerializeField] private float mouseSpeedV;
		
		/// <summary>
		/// Di quanto spostare il target prima di guardare in quella direzione;
		/// </summary>
		[SerializeField] private Vector3 lookAtOffset;
		
		private Vector3 positionDiff;
		private Quaternion rotation;
		protected Transform target { get; private set; }
		private int avoidLayersMask;
		
		/// <summary>
		/// In caso mouseMoveX sia abilitato, questo è il valore di base.
		/// </summary>
		private float flatRotationV;
		
		/// <summary>
		/// If this is going back from the avoidWall to the normal stance.
		/// </summary>
		private bool goingBack;
		
		/// <summary>
		/// Target position, to be lerped to.
		/// </summary>
		private Vector3 positionTarget;
		private Quaternion rotationTarget;
		
		//
		
		private Vector3 vec3;
		
		protected override void Awake ()
		{
			base.Awake();
			
			foreach (string layerName in avoidLayers)
			{
				avoidLayersMask = avoidLayersMask | (1 << LayerMask.NameToLayer(layerName));
			}
		}
		protected override void Start ()
		{
			base.Start();
			
			rotation = transform.rotation;
			flatRotationV = rotation.x;
			
			if ( startingTarget )
			{
				SetTarget(startingTarget);
				
				if (autoSetPositionDiff)
				{
					SetPositionDiff(transform.position - startingTarget.position);
				}
			}
		}
		protected override bool LateUpdate ()
		{
			if ( base.LateUpdate() )
				return true;
			
			if (!target)
				return true;
			
			// position and rotation
			
			if ( rotationBehaviour == RotationBehaviour.Follow || rotationBehaviour == RotationBehaviour.LookAt )
			{
				positionTarget = target.TransformPoint(positionDiff);
			}
			else
			{
				positionTarget = target.position + positionDiff;
			}
			
			// rotation
			
			if (rotationBehaviour == RotationBehaviour.Follow)
			{
				rotationTarget = target.rotation;
			}
			else
			{
				if (rotationBehaviour == RotationBehaviour.Fixed)
				{
					rotationTarget = rotation;
				}
				else if (rotationBehaviour == RotationBehaviour.LookAt)
				{
					rotationTarget = Quaternion.LookRotation(target.position + lookAtOffset - positionTarget);
				}
				else if (rotationBehaviour == RotationBehaviour.Ignore)
				{
					rotationTarget = transform.rotation;
				}
				else
				{
					throw new System.ApplicationException("Unexpected rotation behaviour: " + rotationBehaviour);
				}
			}
			
			// going back
			
			if ( Vector3.Distance(transform.position,positionTarget) < goingBackSnapping )
			{
				goingBack = false;
			}
			
			// obstacle avoidance
			
			if (avoidLayersMask != 0)
			{
				if ( Physics.Linecast(target.position, positionTarget, avoidLayersMask, QueryTriggerInteraction.Ignore) )
				{
					positionTarget = target.position + avoidWallsPosition;
					goingBack = true;
				}
			}
			
			// lerp and set
			
			float actualSmoothing = goingBack ? smoothing : 1f;
			transform.position = Vector3.Lerp(transform.position, positionTarget, actualSmoothing);
			
			if (mouseMoveV)
			{
				float mx = Input.GetAxisRaw("Mouse Y"); // sì, Y
				
				vec3 = rotationTarget.eulerAngles;
				vec3.x = transform.rotation.eulerAngles.x - mx * mouseSpeedV * Time.deltaTime;
				
				rotationTarget = Quaternion.Euler(vec3);
			}
			
			transform.rotation = Quaternion.Lerp(transform.rotation, rotationTarget, actualSmoothing);
			
			return false;
		}
		
		public virtual void SetTargetNull ()
		{
			target = null;
		}
		public virtual void SetTarget (Transform newTarget)
		{
			target = newTarget;
		}
		public virtual void SetPositionDiff (Vector3 diff)
		{
			positionDiff = diff;
		}
		public void ResetRotationV ()
		{
			vec3 = rotationTarget.eulerAngles;
			vec3.x = flatRotationV;
			rotationTarget = Quaternion.Euler(vec3);
		}
	}
}
