﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama.Cameras
{
	public class MoveCamera : MonoBehaviour
	{
		[SerializeField] private float speedH;
		[SerializeField] private float speedV;
		[SerializeField] private float speedZ;
		[SerializeField] private string cameraH;
		[SerializeField] private string cameraV;
		[SerializeField] private string cameraZ;
		
		private Camera cam;
		//private FollowTarget cameraFollow;
		
		void Awake ()
		{
			//cameraFollow = GetComponent<FollowTarget>();
			cam = GetComponent<Camera>();
			if (!cam)
				cameraZ = "";
		}
		void Update ()
		{
			float moveH,moveV,moveZ;
			
			moveH = cameraH.Length>0 ? Input.GetAxis(cameraH) : 0;
			moveV = cameraV.Length>0 ? Input.GetAxis(cameraV) : 0;
			moveZ = cameraZ.Length>0 ? Input.GetAxis(cameraZ) : 0;
			
			if (moveH!=0 || moveV!=0)
			{
				//if (cameraFollow)
				//	cameraFollow.SetTargetNull();
				
				transform.position += Vector3.right * moveH * speedH * Time.deltaTime;
				transform.position += Vector3.forward * moveV * speedV * Time.deltaTime;
			}
			if (moveZ!=0)
			{
				cam.orthographicSize += moveZ * speedZ * Time.deltaTime;
			}
		}
	}
}
