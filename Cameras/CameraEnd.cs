﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2014-2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Goblinsama.Cameras
{
	/// <summary>
	/// Questa classe serve a impedire alla Camera di inquadrare oltre un punto predeterminato.
	/// 
	/// • Innanzi tutto bisogna scegliere un "lato", ovvero se si vuole che non vada "più a destra di così", si scelga pivot:"right".
	/// • Poi bisogna spostare questo oggetto alle coordinate volute.
	/// • Quindi bisogna aggiustare la size (l'asse corrispondente al pivot sarà settato automaticamente, mentre l'altro asse è indicato da questo valore).
	/// 
	/// Il LAYER è impostato automaticamente a "Map", perché deve interagire solo col Player.
	/// Sebbene anche "Interactive" collida solo col Player, ho dovuto creare un layer apposito per i raycast di Map.Refresh().
	/// 
	/// • Fare attenzione a NON SOVRAPPORRE due CameraEnd con lo stesso asse, mentre se si sovrappongono due con asse diverso va bene.
	/// • L'utilizzo principale è metterlo CONTRO UN MURO.
	/// • L'altro utilizzo è di metterli AFFIANCATI: un left subito a destra di un right. Così facendo avremo una "porta" che blocca la visuale
	///   in ambo le direzioni.
	/// 
	/// O lo metti contro un muro, **O** ne metti due opposti affiancati: se ne metti uno singolo in mezzo a una stanza, avrai un effetto orrendo
	/// quando "torni indietro".
	/// </summary>
	
	public class CameraEnd : MonoBehaviourE
	{
		public enum Side { top, right, bottom, left };
		
		public Side pivot;
		public float size;
		
		private BoxCollider2D box;
		private PixelCamera cameraFollow;
		private CameraEnd cameraBlocker {
			get {
				if (pivot==Side.left)
				{
					return cameraFollow.cameraBlockers[(int)Side.left];
				}
				if (pivot==Side.right)
				{
					return cameraFollow.cameraBlockers[(int)Side.right];
				}
				if (pivot==Side.bottom)
				{
					return cameraFollow.cameraBlockers[(int)Side.bottom];
				}
				if (pivot==Side.top)
				{
					return cameraFollow.cameraBlockers[(int)Side.top];
				}
				throw new System.ApplicationException("Unexpected Side");
			}
			set {
				if (pivot==Side.left)
				{
					cameraFollow.cameraBlockers[(int)Side.left]=value;
					return;
				}
				if (pivot==Side.right)
				{
					cameraFollow.cameraBlockers[(int)Side.right]=value;
					return;
				}
				if (pivot==Side.bottom)
				{
					cameraFollow.cameraBlockers[(int)Side.bottom]=value;
					return;
				}
				if (pivot==Side.top)
				{
					cameraFollow.cameraBlockers[(int)Side.top]=value;
					return;
				}
				throw new System.ApplicationException("Unexpected Side");
			}
		}
		
		[System.NonSerialized] public float blockPosition;
		
		// recycle
		private Vector2 vec2,ve2b;
		
		protected override void Awake ()
		{
			Debug.LogError("This class should be updated");
			
			base.Awake();
			
			box = gameObject.AddComponent<BoxCollider2D>();
			box.isTrigger = true;
		}
		protected override void OnEnable ()
		{
			base.OnEnable();
			
			cameraFollow = FindObjectOfType<PixelCamera>();
			UpdatedResolution(new Vector2(Screen.width,Screen.height));
		}
		
		protected override bool OnTriggerEnter2D (Collider2D col)
		{
			if (base.OnTriggerEnter2D(col))
				return true;
			
			if (col.tag!="Character")
				return true;
			
			if (!IsTarget(col.gameObject))
				return true;
			
			if (cameraBlocker==this)
			{
				/* Debug.LogWarning(this+": strange double trigger");
				 * Con la vecchia rete non doveva mai finire qui dentro.
				 * Con le modifiche attuali, entra qui dentro ogni volta che, stando dentro tale trigger, cambia lo stato di grounded.
				 * Questo perché in quello che viene disabilitato non viene chiamato l'Exit, mentre in quello che viene abilitato viene chiamato l'Enter.
				 * Credo.
				 * Dunque in ogni caso basta ignorarlo e gg.
				 * Pare.
				 */
				return true;
			}
			if (cameraBlocker)
			{
				Debug.LogError(this+": double blocker <"+cameraBlocker+">");
				return true;
			}
			
			cameraBlocker=this;
			return false;
		}
		protected override bool OnTriggerExit2D (Collider2D col)
		{
			if (base.OnTriggerExit2D(col))
				return true;
			
			if (col.tag!="Character")
				return true;
			
			if (!IsTarget(col.gameObject))
				return true;
			
			cameraBlocker=null;
			return false;
		}
		
		/* TODO: ripristinare
		protected virtual void OnLevelWasLoaded (int level)
		{
			Refresh();
		}
		*/
		
		protected override void OnDrawGizmos ()
		{
			// TODO, calcolare e disegnare il box intero
			
			Gizmos.color=Color.blue;
			
			if (pivot==Side.left || pivot==Side.right)
			{
				vec2.Set( transform.position.x, transform.position.y-size/2 );
				ve2b.Set( transform.position.x, transform.position.y+size/2 );
			}
			else
			{
				vec2.Set( transform.position.x-size/2, transform.position.y );
				ve2b.Set( transform.position.x+size/2, transform.position.y );
			}
			Gizmos.DrawLine(vec2,ve2b);
		}
		
		void UpdatedResolution (Vector2 size)
		{
			Refresh((int)size.x,(int)size.y);
		}
		
		protected virtual bool IsTarget (GameObject go)
		{
			return (cameraFollow!=null && cameraFollow.target!=null && cameraFollow.target==go.transform);
		}
		private void Refresh ()
		{
			Refresh(0,0);
		}
		private void Refresh (int width, int height)
		{
			if (cameraFollow==null)
			{
				cameraFollow=FindObjectOfType<PixelCamera>();
			}
			if (width==0)
			{
				width=Screen.width;
			}
			if (height==0)
			{
				height=Screen.height;
			}
			
			float unit = height/(FindObjectOfType<PixelCamera>().cameraSelf.orthographicSize*2);
			//Debug.Log(string.Format("camera: {3}, res: {0}x{1}, unit: {2}",width,height,unit,cameraFollow));
			
			if (pivot==Side.left || pivot==Side.right)
			{
				// size indica l'altezza, dobbiamo aggiustare la larghezza
				
				// NOTA: il collider del player, in quanto idealmente dovrebbe bloccarsi solo quando il suo centro entra nel box
				float sub = 0;// TODO, GameManager.player.GetComponent<Collider2D>().bounds.size.x/2;
				vec2.Set( width/unit/2-sub, size ); // TODO, non funziona perfettamente, non so perché
				box.size = vec2;
				
				vec2.Set( pivot==Side.left ? box.size.x/2 : -box.size.x/2, 0 );
				box.offset = vec2;
			}
			else
			{
				// viceversa
				
				// NOTA: sarebbe più carino fare automaticamente tale controllo basandosi sul pivot, ma non è banale e chissene
				float sub = 0;// TODO, pivot==Side.bottom ? 0 : GameManager.player.GetComponent<Collider2D>().bounds.size.y; // TODO, UNTESTED
				vec2.Set( size, height/unit/2-sub );
				box.size = vec2;
				
				vec2.Set( 0, pivot==Side.bottom ? box.size.y/2 : -box.size.y/2 );
				box.offset = vec2;
			}
			
			if (pivot==Side.left)
			{
				blockPosition = box.bounds.max.x;
			}
			else if (pivot==Side.right)
			{
				blockPosition = box.bounds.min.x;
			}
			else if (pivot==Side.bottom)
			{
				blockPosition = box.bounds.max.y;
			}
			else if (pivot==Side.top)
			{
				blockPosition = box.bounds.min.y;
			}
		}
	}
}
