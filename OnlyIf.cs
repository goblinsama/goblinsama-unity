﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace Goblinsama
{
	public class OnlyIf : MonoBehaviourE
	{
		public enum Condition
		{
			IfClient,
			IfServer,
			IfAdmin,
			IfDebug,
			IfDesktop,
			IfMobile,
			IfRelease,
			IfInGame,
			IfNotInGame,
			IfEditor,
			IfNotEditor,
		}
		
		[SerializeField] private Condition showIf;
		
		protected override void OnEnable ()
		{
			if ( !Director.active )
				return;
			
			base.OnEnable();
			ShowOnCondition();
		}
		
		protected virtual bool AmAdmin ()
		{
			return false;
		}
		protected virtual bool InGame ()
		{
			return false;
		}
		protected virtual bool NotInGame ()
		{
			return false;
		}
		
		public bool ShowOnCondition ()
		{
			bool show;
			
			switch ( showIf )
			{
				case Condition.IfClient:
					show = NetworkDirector.amClient;
				break;
				
				case Condition.IfServer:
					show = NetworkDirector.amServer;
				break;
				
				case Condition.IfAdmin:
					show = AmAdmin();
				break;
				
				case Condition.IfInGame:
					show = InGame();
				break;
				
				case Condition.IfNotInGame:
					show = NotInGame();
				break;
				
				case Condition.IfDebug:
					show = Debug.isDebugBuild;
				break;
				
				case Condition.IfRelease:
					show = !Debug.isDebugBuild;
				break;
				
				case Condition.IfMobile:
					#if UNITY_ANDROID || UNITY_IOS || UNITY_WP_8_1
					show = true;
					#else
					show = false;
					#endif
				break;
				
				case Condition.IfDesktop:
					#if UNITY_STANDALONE
					show = true;
					#else
					show = false;
					#endif
				break;
				
				case Condition.IfEditor:
					#if UNITY_EDITOR
					show = true;
					#else
					show = false;
					#endif
				break;
				
				case Condition.IfNotEditor:
					#if UNITY_EDITOR
					show = false;
					#else
					show = true;
					#endif
				break;
				
				#pragma warning disable 0162
				default:
					throw new System.NotImplementedException( "Unknown Condition: " + showIf );
				break;
				#pragma warning restore 0162
			}
			
			gameObject.SetActive( show );
			
			return show;
		}
	}
}
