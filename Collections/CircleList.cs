﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using System.Collections.Generic;
using System.Collections;

namespace Goblinsama
{
	public class CircleList<T> : ICollection<T>
	{
		protected int cursor;
		protected List<T> list;
		protected bool debug = false;
		
		protected void Init ( List<T> source )
		{
			list = new List<T>(source);
			Reset();
		}
		protected void Init ( T[] source )
		{
			list = new List<T>(source);
			Reset();
		}
		public CircleList ()
		{
			list = new List<T>();
			Reset();
		}
		public CircleList ( List<T> source )
		{
			Init(source);
		}
		public CircleList ( T[] source )
		{
			Init(source);
		}
		public void DebugActivate ()
		{
			debug = true;
		}
		
		public T GetNext ()
		{
			AdvanceCursor();
			if (debug)
			{
				UnityEngine.Debug.Log(this + " get next: cursor " + cursor);
			}
			return list[cursor];
		}
		public void Reset ()
		{
			cursor = -1;
		}
		public void Add ( T elem )
		{
			list.Add( elem );
		}
		
		/// <summary>
		/// Finds the nearest element, returns it and moves the cursor accordingly.
		/// </summary>
		public virtual T SkipToNearest (T nearTo)
		{
			throw new System.NotImplementedException();
		}
		
		private void AdvanceCursor ()
		{
			cursor = (cursor + 1) % list.Count;
		}
		private void SanityCheck ()
		{
			cursor = cursor % list.Count;
		}
		
		#region ICollection
		
		public int Count {
			get {
				return list.Count;
			}
		}
		public bool IsReadOnly {
			get {
				return false;
			}
		}
		public void Clear ()
		{
			list.Clear();
			Reset();
		}
		public bool Contains ( T elem )
		{
			return list.Contains( elem );
		}
		public void CopyTo ( T[] dest, int from )
		{
			list.CopyTo( dest, from );
		}
		public bool Remove ( T elem )
		{
			bool ret = list.Remove( elem );
			if ( ret )
				SanityCheck();
			return ret;
		}
		IEnumerator IEnumerable.GetEnumerator ()
		{
			return this.GetEnumerator();
		}
		public IEnumerator<T> GetEnumerator ()
		{
			throw new System.NotImplementedException();
		}
		
		#endregion
	}
}
