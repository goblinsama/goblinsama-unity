﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections.Generic;

namespace Goblinsama
{
	public class CircleListVector3 : CircleList<Vector3>
	{
		public CircleListVector3 () : base() {}
		public CircleListVector3 ( List<Vector3> source ) : base(source) {}
		public CircleListVector3 ( Vector3[] source ) : base(source) {}
		
		public override Vector3 SkipToNearest (Vector3 nearTo)
		{
			if ( list.Count < 1 )
				throw new System.ApplicationException("Can't call SkipToNearest on an empty list");
			
			float minDistance = float.MaxValue;
			int found = -1;
			
			for (int idx = 0; idx < list.Count; idx++)
			{
				float distance = Vector3.Distance(nearTo, list[idx]);
				if ( distance < minDistance )
				{
					found = idx;
					minDistance = distance;
				}
			}
			
			//UnityEngine.Debug.Log(this + " found id " + found + " at " + list[found]);
			cursor = found;
			return list[found];
		}
	}
}
