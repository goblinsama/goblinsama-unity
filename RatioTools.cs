﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using System.Linq;

namespace Goblinsama
{
	/// <summary> Utilità per fare il calcolo del rapporto dello schermo come frazione. </summary>
	public static class RatioTools
	{
		/// <summary> Aggiorna le frazioni con numeri più noti. </summary>
		/// <param name="numbers"> I due numeri da controllare. </param>
		public static void Complify (int[] numbers)
		{
			if (numbers[0] == 8 && numbers[1] == 5)
			{
				numbers[0] *= 2;
				numbers[1] *= 2;
				return;
			}
			if (numbers[0] == 5 && numbers[1] == 3)
			{
				numbers[0] *= 3;
				numbers[1] *= 3;
				return;
			}
		}
		
		/// <summary>
		/// Semplifica i due numeri per avere la frazione minima. 
		/// http://stackoverflow.com/a/2941224/207655.
		/// </summary>
		/// <param name="numbers"> l'array dei due numeri da semplificare. </param>
		public static void Simplify (int[] numbers)
		{
			int gcd = GCD(numbers);
			for (int i = 0; i < numbers.Length; i++)
				numbers[i] /= gcd;
		}
		
		/// <summary> Calcola il massimo comun divisore. </summary>
		/// <param name="a"> il primo numero. </param>
		/// <param name="b"> il secondo numero. </param>
		/// <returns> il massimo comun divisore. </returns>
		public static int GCD (int a, int b)
		{
			while (b > 0)
			{
				int rem = a % b;
				a = b;
				b = rem;
			}
			return a;
		}
		
		/// <summary> Calcola il massimo comun divisore. </summary>
		/// <param name="args"> l'array dei due numeri per i quali cercare il MCD. </param>
		/// <returns> il massimo comun divisore. </returns>
		public static int GCD (int[] args)
		{
			return args.Aggregate((gcd, arg) => GCD(gcd, arg));
		}
	}
}
