﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

#if UNITY_EDITOR

using UnityEngine;
using UnityEditor;

namespace Goblinsama
{
	public static class Snap
	{
		public static float snapVal = .5f;
		public static float snapRot = 90;
		
		public static void SnapPosition (bool snapH, bool snapV)
		{
			Vector3 pos;
			foreach (GameObject go in Selection.gameObjects)
			{
				// posizione
				
				pos = go.transform.localPosition;
				if (snapH)
				{
					pos.x = RoundTo(pos.x, snapVal);
					pos.z = RoundTo(pos.z, snapVal);
				}
				if (snapV)
				{
					pos.y = RoundTo(pos.y, snapVal);
				}
				go.transform.localPosition = pos;
				
				// rotazione
				
				Vector3 rotEul = go.transform.localRotation.eulerAngles;
				rotEul.x = RoundTo(rotEul.x, snapRot);
				rotEul.y = RoundTo(rotEul.y, snapRot);
				rotEul.z = RoundTo(rotEul.z, snapRot);
				go.transform.localRotation = Quaternion.Euler(rotEul);
			}
		}
		
		private static float RoundTo (float num, float round)
		{
			return Mathf.Round(num / round) * round;
		}
	}
}

#endif
