﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.SceneManagement;

namespace Goblinsama
{
	public class LoadScene : MonoBehaviourE
	{
		/// <summary>
		/// Se sta già procedendo a fare una load0.
		/// </summary>
		protected static bool loading0;
		
		/// <summary>
		/// Carica la scena 0 se non lo è già.
		/// </summary>
		[SerializeField] protected bool load0;
		
		/// <summary>
		/// Carica le scene indicate, in modo Additive.
		/// </summary>
		[SerializeField] protected string[] loadAdditive;
		
		/// <summary>
		/// Questa è la scena 0.
		/// Ciò è assolutamente idiota ma non ci si può far niente, dato che SceneIdIsLoaded non funziona.
		/// </summary>
		[SerializeField] protected bool thisIsScene0;
		
		protected override void Awake ()
		{
			base.Awake();
			
			#if UNITY_EDITOR
			if ( thisIsScene0 )
			{
				loading0 = true;
			}
			
			if ( !loading0 )
			{
				foreach ( LoadScene ls in FindObjectsOfType<LoadScene>() )
				{
					if ( ls != this )
						throw new UnityException( "Can't have two LoadScene objects." );
				}
				
				if ( load0 )
				{
					if ( !SceneIdIsLoaded( 0 ) ) // TODO-. non funziona, entra sempre anche quando non dovrebbe
					{
						loading0 = true;
						foreach ( DontDestroy go in FindObjectsOfType<DontDestroy>() )
						{
							Destroy( go.gameObject );
						}
						SceneManager.LoadScene( 0, LoadSceneMode.Single );
						return;
					}
				}
			}
			#endif
			
			foreach ( string sceneName in loadAdditive )
			{
				if ( !SceneNameIsLoaded( sceneName ) )
				{
					SceneManager.LoadScene( sceneName, LoadSceneMode.Additive );
				}
			}
		}
		
		private bool _SceneIsLoaded (bool checkId, int sceneBuildId, bool checkName, string sceneName)
		{
			for (int si=0; si<SceneManager.sceneCount; si++)
			{
				Scene sc = SceneManager.GetSceneAt(si);
				//Debug.Log(string.Format("{0}# {1} : {2} .",SceneManager.GetActiveScene().name,sc.buildIndex,sc.name));
				
				if (checkName && (sc.name == sceneName))
					return true;
				if (checkId && (sc.buildIndex == sceneBuildId))
					return true;
			}
			return false;
		}
		private bool SceneNameIsLoaded (string sceneName)
		{
			return _SceneIsLoaded(false,-2,true,sceneName);
		}
		private bool SceneIdIsLoaded (int sceneBuildId)
		{
			return _SceneIsLoaded(true,sceneBuildId,false,null);
		}
	}
}
