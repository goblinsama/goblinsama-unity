﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.SceneManagement;

namespace Goblinsama
{
	public class LoadGame : MonoBehaviourE
	{
		public enum Type
		{
			Timeout, // attesa di tot secondi
			WaitForInput, // attende un qualunque input per proseguire
			SkippableTimeout, // Timeout che può essere skippato premendo un tasto
		}
		
		[SerializeField] private Type type;
		[SerializeField] private string loadImmediately = "Directors";
		[SerializeField] private string loadSceneName = "GameScene";
		[SerializeField] private string[] loadScenesAdditive;
		[SerializeField] private GameObject enableOnProceed;
		
		/// <summary>
		/// Tempo passato il quale il gioco semplicemente parte.
		/// </summary>
		[SerializeField][Range(2f,12f)] private float waitTimeMax = 8f;
		
		/// <summary>
		/// Tempo minimo di attesa prima che ti faccia skippare (la prima volta).
		/// </summary>
		[SerializeField][Range(2f,12f)] private float waitTimeMed = 3f;
		
		/// <summary>
		/// Tempo minimo di attesa prima che ti faccia skippare (se hai già skippato una volta).
		/// </summary>
		[SerializeField][Range(0f,12f)] private float waitTimeMin = 1f;
		
		private float startedTime;
		private bool skippedOnce;
		private bool proceeding;
		
		private bool useTimeout {
			get {
				return (type == Type.Timeout || type==Type.SkippableTimeout);
			}
		}
		private bool useInput {
			get {
				return (type == Type.WaitForInput || type==Type.SkippableTimeout);
			}
		}
		private float actualWaitTimeMin {
			get {
				return skippedOnce ? waitTimeMin : waitTimeMed;
			}
		}
		
		protected override void Start ()
		{
			base.Start();
			
			skippedOnce = PlayerPrefs.GetInt("skippedIntro",0)>0;
			
			if (waitTimeMax < waitTimeMin)
				waitTimeMax = waitTimeMin;
			
			startedTime = Time.time;
			proceeding = false;
			
			if ( loadImmediately.Length > 0 )
				SceneManager.LoadScene(loadImmediately, LoadSceneMode.Additive);
		}
		protected override bool Update ()
		{
			if ( base.Update() )
				return true;
			
			if ( proceeding )
				return true;
			
			if (useTimeout && (startedTime + waitTimeMax < Time.time))
			{
				//Debug.Log("timeout");
				
				Proceed();
				return true;
			}
			
			if (useInput && (startedTime + actualWaitTimeMin < Time.time) && Input.anyKey)
			{
				//Debug.Log("skipping");
				
				PlayerPrefs.SetInt("skippedIntro",1);
				PlayerPrefs.Save();
				
				Proceed();
				return true;
			}
			
			return false;
		}
		
		private void Proceed ()
		{
			//Debug.Log("proceeding");
			
			proceeding = true;
			
			if ( enableOnProceed )
				enableOnProceed.SetActive(true);
			
			foreach (string sceneName in loadScenesAdditive)
			{
				SceneManager.LoadScene(sceneName, LoadSceneMode.Additive);
			}
			if ( loadSceneName.Length > 0 )
			{
				SceneManager.LoadScene(loadSceneName, LoadSceneMode.Single);
			}
		}
	}
}
