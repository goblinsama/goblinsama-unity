﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace Goblinsama
{
	/// <summary>
	/// Improved MonoBehaviour:
	///  • it fixes the problem of OnDestroy called during ApplicationQuit
	///  • it adds a cached .rectTransform link
	/// </summary>
	public class MonoBehaviourQ : MonoBehaviour
	{
		protected bool quitting { get; private set; }
		protected bool destroying { get; private set; }
		protected bool disabling { get; private set; }
		
		public bool isTerminating {
			get {
				return quitting || destroying || disabling;
			}
		}
		
		protected RectTransform _rectTransform;
		public RectTransform rectTransform {
			get {
				if ( _rectTransform == null )
					_rectTransform = GetComponent<RectTransform>();
				return _rectTransform;
			}
		}
		
		private bool initialisedUI = false;
		protected Rect interfaceScreenPos { get; private set; }
		
		public Vector2 screenPosMin {
			get {
				return rectTransform.TransformPoint( rectTransform.rect.min );
			}
		}
		public Vector2 screenPosMax {
			get {
				return rectTransform.TransformPoint( rectTransform.rect.max );
			}
		}
		
		protected virtual void OnEnable ()
		{
			disabling = false;
		}
		protected virtual void OnApplicationQuit ()
		{
			quitting = true;
		}
		protected virtual bool OnDestroy ()
		{
			if ( quitting )
				return true;
			
			destroying = true;
			return false;
		}
		protected virtual bool OnDisable ()
		{
			if ( quitting )
				return true;
			
			disabling = true;
			return false;
		}
		
		private void InitialiseUI ()
		{
			if ( initialisedUI )
				return;
			
			RefreshInitUI();
		}
		
		public void RefreshInitUI ()
		{
			interfaceScreenPos = new Rect( screenPosMin, screenPosMax - screenPosMin );
			
			initialisedUI = true;
		}
		public bool IsMouseIn ()
		{
			InitialiseUI();
			
			return interfaceScreenPos.Contains( Input.mousePosition );
		}
	}
}
