﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.SceneManagement;

namespace Goblinsama
{
	public abstract class MonoBehaviourL : MonoBehaviourE
	{
		protected override void OnEnable ()
		{
			base.OnEnable();
			
			SceneManager.sceneLoaded += OnLevelLoaded;
		}
		protected override bool OnDisable ()
		{
			if ( base.OnDisable() )
				return true;
			
			SceneManager.sceneLoaded -= OnLevelLoaded;
			
			return false;
		}
		
		protected abstract void OnLevelLoaded ( Scene scene, LoadSceneMode mode );
	}
}
