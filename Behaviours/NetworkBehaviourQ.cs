﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace Goblinsama
{
	/// <summary>
	/// Vedi MonoBehaviourQ.
	/// </summary>
	public class NetworkBehaviourQ : NetworkBehaviour
	{
		private bool quitting = false;
		private bool destroying = false;
		private bool disabling = false;
		
		public bool isTerminating {
			get {
				return quitting||destroying||disabling;
			}
		}
		
		protected virtual void OnEnable ()
		{
			disabling = false;
		}
		protected virtual void OnApplicationQuit ()
		{
			quitting = true;
		}
		protected virtual bool OnDestroy ()
		{
			if (quitting)
				return true;
			
			destroying = true;
			return false;
		}
		protected virtual bool OnDisable ()
		{
			if (quitting)
				return true;
			
			disabling = true;
			return false;
		}
	}
}
