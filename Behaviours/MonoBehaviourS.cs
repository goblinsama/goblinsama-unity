﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama
{
	/// <summary>
	/// Estensione di MonoBehaviourE, che include anche la SlowUpdate.
	/// </summary>
	public class MonoBehaviourS : MonoBehaviourE
	{
		public enum Method
		{
			InvokeRepeating,
			FixedUpdate,
		}
		
		[SerializeField] private float slowUpdateFreq;
		[SerializeField] private Method method = Method.InvokeRepeating;
		
		private float lastUpdate;
		
		protected virtual float SlowUpdateFrequency ()
		{
			return float.NaN;
		}
		
		protected override void Start ()
		{
			base.Start();
			
			if ( slowUpdateFreq <= 0 )
			{
				slowUpdateFreq = SlowUpdateFrequency();
				if ( float.IsNaN( slowUpdateFreq ) )
				{
					Debug.LogWarning( this + ": no slow update frequency set." );
					return;
				}
			}
			
			if ( slowUpdateFreq < Time.fixedDeltaTime )
			{
				Debug.LogError( this + "slowUpdateFreq can't be < than fixedDeltaTime." );
				return;
			}
			
			if ( method == Method.InvokeRepeating )
			{
				InvokeRepeating( "SlowUpdate", slowUpdateFreq, slowUpdateFreq );
			}
		}
		
		protected virtual bool SlowUpdate ()
		{
			lastUpdate = Time.time;
			return false;
		}
		
		protected override bool FixedUpdate ()
		{
			if ( base.FixedUpdate() )
				return true;
			
			if ( ( method == Method.FixedUpdate ) && !float.IsNaN( slowUpdateFreq ) && ( lastUpdate + slowUpdateFreq < Time.time ) )
			{
				SlowUpdate();
			}
			
			return false;
		}
	}
}
