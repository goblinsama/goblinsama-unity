﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama
{
	/// <summary>
	/// MonoBehaviour che inoltre "unifica" le OnCollisionEnter e le OnTriggerEnter in una generica OnHit
	/// </summary>
	public class MonoBehaviourC : MonoBehaviourE
	{
		protected override sealed bool OnCollisionEnter (Collision col)
		{
			if (base.OnCollisionEnter(col))
				return true;
			return _OnHit(col,col.collider);
		}
		protected override sealed bool OnTriggerEnter (Collider col)
		{
			if (base.OnTriggerEnter(col))
				return true;
			return _OnHit(null,col);
		}
		protected override bool OnControllerColliderHit (ControllerColliderHit col)
		{
			if (base.OnControllerColliderHit(col))
				return true;
			return _OnHit(null,col.collider);
		}
		private bool _OnHit (Collision collision, Collider collider)
		{
			return OnHit(collision, collider, (collision != null) ? collision.contacts[0].point : collider.transform.position);
		}
		protected virtual bool OnHit (Collision collision, Collider collider, Vector3 hitPoint)
		{
			return false;
		}
	}
}
