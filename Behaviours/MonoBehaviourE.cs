﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama
{
	/// <summary>
	/// MonoBehaviour esteso, che oltre a incorporare il fix della OnDestroy, fornisce le funzioni base di Unity da overridare.
	/// Ciò ha il solo scopo di aumentare l'ordine, essendo che avrebbe dovuto essere Unity stesso a strutturarle in questo modo.
	/// </summary>
	public class MonoBehaviourE : MonoBehaviourQ
	{
		protected virtual void Awake () {}
		protected virtual void Start () {}
		
		/// <summary>
		/// Chiamarla all'inizio dell'override: se torna true, uscire subito tornando true.
		/// Se torna false, proseguire l'esecuzione e infine tornare false.
		/// </summary>
		protected virtual bool Update () { return false; }
		protected virtual bool FixedUpdate () { return false; }
		protected virtual bool LateUpdate () { return false; }
		protected virtual void OnDrawGizmos () {}
		
		protected virtual bool OnApplicationFocus ( bool hasFocus ) { return false; }
		
		protected virtual bool OnTriggerEnter ( Collider col ) { return false; }
		protected virtual bool OnTriggerExit ( Collider col ) { return false; }
		protected virtual bool OnTriggerStay ( Collider col ) { return false; }
		protected virtual bool OnCollisionEnter ( Collision col ) { return false; }
		protected virtual bool OnCollisionExit ( Collision col ) { return false; }
		protected virtual bool OnCollisionStay ( Collision col ) { return false; }
		protected virtual bool OnTriggerEnter2D ( Collider2D col ) { return false; }
		protected virtual bool OnTriggerExit2D ( Collider2D col ) { return false; }
		protected virtual bool OnTriggerStay2D ( Collider2D col ) { return false; }
		protected virtual bool OnCollisionEnter2D ( Collision2D col ) { return false; }
		protected virtual bool OnCollisionExit2D ( Collision2D col ) { return false; }
		protected virtual bool OnCollisionStay2D ( Collision2D col ) { return false; }
		protected virtual bool OnControllerColliderHit ( ControllerColliderHit col ) { return false; }
	}
}
