﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.Effects
{
	public class OnFinish : Effect
	{
		[SerializeField] private GameObject[] finishEnable;
		[SerializeField] private GameObject[] finishDisable;
		[SerializeField] private GameObject[] finishDestroy;
		
		protected override bool Finish ()
		{
			if ( base.Finish() )
				return true;
			
			foreach (GameObject go in finishEnable)
			{
				go.SetActive(true);
			}
			foreach (GameObject go in finishDisable)
			{
				go.SetActive(false);
			}
			foreach (GameObject go in finishDestroy)
			{
				Destroy(go);
			}
			
			return false;
		}
	}
}
