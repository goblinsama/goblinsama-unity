﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace Goblinsama
{
	public class Shrink : NetworkBehaviour
	{
		[SerializeField] private Vector3 scaleTarget;
		[SerializeField] private float duration;
		[SerializeField] private GameObject destroyOnFinish;
		
		private Vector3 initialScale;
		private float started; // NOTA: ho provato a Syncare questo invece che usare syncScale, e non funzionava
		[SyncVar] private Vector3 syncScale;
		
		void Awake ()
		{
			initialScale = transform.localScale;
		}
		void Update ()
		{
			if ( NetworkDirector.amClient )
			{
				transform.localScale = syncScale;
			}
			
			if ( started <= 0 )
				return;
			
			if ( NetworkDirector.amServer )
			{
				float current = ( Director.time - started ) / duration;
				if ( current > 1 )
				{
					started = 0;
					if ( destroyOnFinish )
					{
						Destroy( destroyOnFinish );
					}
					return;
				}
				
				syncScale = transform.localScale = Vector3.Lerp( initialScale, scaleTarget, current );
			}
		}
		
		public void Do ()
		{
			started = Goblinsama.Director.time;
		}
	}
}
