﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace Goblinsama
{
	public class Vanish : MonoBehaviour
	{
		[SerializeField] private float duration;
		[SerializeField] private GameObject onDestroyCallObject;
		[SerializeField] private string onDestroyCallMessage;
		
		private float created;
		
		void Start ()
		{
			created = Time.time;
		}
		void Update ()
		{
			if ( !NetworkDirector.amServer )
				return;
			
			if ( Time.time > created + duration )
			{
				Destroy( gameObject );
			}
		}
		void OnDestroy ()
		{
			if ( NetworkDirector.amServer )
			{
				if ( onDestroyCallObject != null && onDestroyCallMessage.Length > 0 )
				{
					onDestroyCallObject.SendMessage( onDestroyCallMessage ); // NOTA: prima c'era un "don't require", a causa di una volta che si era piantato con due pg uccisisi a vicenda
				}
			}
		}
	}
}
