﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using Goblinsama;

namespace Goblinsama.Effects
{
	public abstract class Effect : MonoBehaviourE
	{
		[SerializeField] private float _duration;
		[SerializeField] protected GameObject target;
		
		protected float started { get; private set; }
		protected float perc { get; private set; }
		private bool finished;
		
		protected float duration {
			get {
				return _duration;
			}
		}
		
		protected override void Start ()
		{
			base.Start();
			Reset();
		}
		protected override bool Update ()
		{
			if ( base.Update() )
				return true;
			
			if ( finished )
				return true;
			
			perc = (Director.time - started) / duration;
			
			if ( perc > 1 )
			{
				Finish();
				return true;
			}
			
			return false;
		}
		
		protected virtual bool Finish ()
		{
			if ( finished )
				return true;
			
			finished = false;
			
			return false;
		}
		protected virtual void Reset ()
		{
			started = Director.time;
			finished = false;
		}
	}
}
