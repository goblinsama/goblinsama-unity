﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama.Effects
{
	public class Fading : Effect
	{
		[SerializeField] private float targetAlpha;
		
		private Renderer affect;
		private float startingAlpha;
		private Material mat;
		
		// recycle
		
		private Color col;
		
		protected override void Start ()
		{
			base.Start();
			affect = target.GetComponentInChildren<Renderer>();
			
			mat = new Material(affect.material);
			startingAlpha = mat.color.a;
			affect.material = mat;
		}
		protected override bool Update ()
		{
			if ( base.Update() )
				return true;
			
			float val = Mathf.Lerp(startingAlpha, targetAlpha, perc);
			
			col = mat.color;
			col.a = val;
			mat.color = col;
			
			return false;
		}
	}
}
