﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;

namespace Goblinsama.Anim
{
	[RequireComponent(typeof(Image))]
	public class AnimControllerUI : AnimControllerAbstract
	{
		private Image imageUI;
		
		protected override void Awake ()
		{
			base.Awake();
			
			imageUI = GetComponent<Image>();
		}
		
		protected override void SetSprite ( Sprite val )
		{
			imageUI.sprite = val;
		}
		protected override void SetSortingLayer ( string layerName )
		{
			;
		}
		protected override void SetSpritePosition ( Vector3 pos )
		{
			imageUI.transform.localPosition = pos;
		}
		protected override void SetSpriteScale ( Vector3 scale )
		{
			imageUI.transform.localScale = scale;
		}
	}
}
