﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections.Generic;

namespace Goblinsama.Anim
{
	/// <summary>
	/// The sprites of an animation, be it a full Anim, or a simple AnimDirection.
	/// </summary>
	public class AnimSprites : MonoBehaviourE
	{
		[SerializeField] private List<Sprite> sprites;
		
		public List<Sprite> spriteList {
			get {
				return sprites;
			}
			set {
				sprites = value;
			}
		}
	}
}
