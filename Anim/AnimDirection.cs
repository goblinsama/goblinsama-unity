﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.Anim
{
	/// <summary>
	/// An Anim for a single direction.
	/// The Anim class can include multiple AnimDirection, and if it does, it displays the correct one,
	/// based on which direction is the character moving.
	/// </summary>
	public class AnimDirection : AnimSprites
	{
		public Vector2 direction;
	}
}
