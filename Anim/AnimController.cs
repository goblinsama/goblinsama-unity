﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama.Anim
{
	[ExecuteInEditMode]
	public class AnimController : AnimControllerAbstract
	{
		[SerializeField] protected SpriteRenderer spriteRenderer;
		
		private bool _mirror;
		
		public SpriteRenderer sprite {
			get {
				return spriteRenderer;
			}
		}
		
		protected override void Awake ()
		{
			base.Awake();
			
			if ( spriteRenderer == null )
				spriteRenderer = GetComponentInChildren<SpriteRenderer>();
		}
		
		public void Set ( Anim anim, bool flipX, bool flipY, System.Func<bool> callback )
		{
			Set( anim, callback );
			Flip( flipX, flipY );
		}
		public void Set ( Anim anim, bool flipX, bool flipY )
		{
			Set( anim );
			Flip( flipX, flipY );
		}
		public void Flip ( bool flipX, bool flipY )
		{
			spriteRenderer.flipX = flipX;
			spriteRenderer.flipY = flipY;
		}
		
		public override bool Mirror ()
		{
			return _mirror;
		}
		public void SetMirror ( bool val )
		{
			if ( !useAnimPosition )
				throw new System.Exception( "AnimController.mirror requires AnimController.useAnimPosition to be enabled." );
			
			_mirror = val;
			
			UpdateTransform();
		}
		
		protected override void SetSprite ( Sprite val )
		{
			spriteRenderer.sprite = val;
		}
		protected override void SetSortingLayer ( string layerName )
		{
			spriteRenderer.sortingLayerName = layerName;
		}
		protected override void SetSpritePosition ( Vector3 pos )
		{
			spriteRenderer.transform.localPosition = pos;
		}
		protected override void SetSpriteScale ( Vector3 scale )
		{
			spriteRenderer.transform.localScale = scale;
		}
		
		#if UNITY_EDITOR
		
		public void SetLast ()
		{
			current.SetLast();
			spriteRenderer.sprite = current.sprite;
		}
		
		#endif
	}
}
