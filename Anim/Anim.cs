﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2014-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	© 2010,2013 Lorenzo Petrone "Lohoris" <looris+src@gmail.com> https://lohoris.net
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using Goblinsama;

namespace Goblinsama.Anim
{
	[ExecuteInEditMode]
	public class Anim : AnimSprites
	{
		public enum Behaviour {
			Looping,
			Once,
			OnceAndDie,
			OnceAndCallback,
		}
		
		// public
		
		[SerializeField] private List<int> frames; // la durata in frame degli sprite
		public string sortingLayerName = "";
		[SerializeField] private Behaviour behaviour;
		
		/// <summary>
		/// Nell'animazione del potere, indica a quale frame deve effettivamente attivare il potere stesso.
		/// </summary>
		[SerializeField] private Sprite keyFrame;
		
		[System.NonSerialized] public System.Func<bool> powerCallback;
		
		// private
		
		private int currentSprite;
		private int currentFrame;
		private AnimSprites currentAnimDirection;
		
		private AnimDirection[] animDirections;
		
		private bool _animationEnded;
		public bool animationEnded {
			get {
				return _animationEnded;
			}
		}
		public bool cantEndOrDid {
			get {
				return ( behaviour == Behaviour.Looping ) || ( animationEnded );
			}
		}
		
		private List<Sprite> sprites {
			get {
				if ( currentAnimDirection == null )
					currentAnimDirection = this;
				return currentAnimDirection.spriteList;
			}
		}
		public Sprite sprite {
			get {
				return sprites[currentSprite];
			}
		}
		public Sprite firstSprite {
			get {
				return sprites[0];
			}
		}
		public Sprite secondSprite {
			get {
				return sprites.Count > 1 ? sprites[1] : firstSprite;
			}
		}
		public Sprite lastSprite {
			get {
				return sprites[sprites.Count - 1];
			}
		}
		public Sprite midSprite {
			get {
				return sprites[sprites.Count / 2];
			}
		}
		
		#if UNITY_EDITOR
		
		[SerializeField] private bool DEBUG;
		
		#endif
		
		// unity
		
		protected override void Awake ()
		{
			base.Awake();
			
			#if UNITY_EDITOR
			if ( DEBUG )
				Log.Add( Time.frameCount + "# " + this + ".Awake " + sprites.Count );
			#endif
			
			currentAnimDirection = this;
			Reset();
		}
		protected override void Start ()
		{
			base.Start();
			
			animDirections = GetComponentsInChildren<AnimDirection>();
			
			if ( !Application.isPlaying )
				return;
			
			// sanity check
			
			foreach ( AnimDirection adir in animDirections )
			{
				if ( adir.spriteList.Count != spriteList.Count )
				{
					Debug.LogError( this + ": sprite count mismatch with <" + adir + ">, disabling." );
					gameObject.SetActive( false );
					return;
				}
			}
		}
		protected override bool FixedUpdate ()
		{
			if ( base.FixedUpdate() )
				return true;
			
			/* NOTA: sebbene la gestione dell'animazione stessa avrebbe più senso dentro la Update,
			 * ora che si occupa di chiamare anche la callback del potere, è giusto che stia qua.
			 * Diversamente, rischierebbe di perdere il frame giusto.
			 * Non che questo sia un gran metodo, ma finché il metodo è questo, va fatto così.
			 * Non che ci siano ovvi metodi migliori.
			 */
			
			if ( !Application.isPlaying )
				return true;
			
			if ( sprites.Count < 1 )
				return true;
			
			if ( _animationEnded )
			{
				#if UNITY_EDITOR
				if ( DEBUG )
					Log.Add( Time.frameCount + "# " + this + " ended already " + currentSprite );
				#endif
				
				if ( behaviour == Behaviour.OnceAndDie )
				{
					#if UNITY_EDITOR
					if ( DEBUG )
						Log.Add( Time.frameCount + "# " + this + " ended destroy" );
					#endif
					
					Destroy( gameObject );
				}
				else if ( behaviour == Behaviour.OnceAndCallback )
				{
					TryCallback();
				}
				return true;
			}
			
			if ( ++currentFrame >= frames[currentSprite] )
			{
				currentFrame = 0;
				if ( ++currentSprite >= sprites.Count )
				{
					if ( behaviour == Behaviour.Looping )
					{
						currentSprite = 0;
					}
					else
					{
						#if UNITY_EDITOR
						if ( DEBUG )
							Log.Add( Time.frameCount + "# " + this + " ended now " + currentSprite );
						#endif
						
						_animationEnded = true;
						SetLast();
					}
				}
			}
			
			if ( ( keyFrame != null ) && ( keyFrame == sprite ) )
			{
				TryCallback();
			}
			
			return false;
		}
		#if UNITY_EDITOR
		protected override bool Update ()
		{
			if ( base.Update() )
				return true;
			
			if ( frames == null || sprites == null )
				return true;
			
			if ( frames.Count > sprites.Count )
			{
				frames.RemoveRange( sprites.Count, frames.Count - sprites.Count );
			}
			else if ( frames.Count < sprites.Count )
			{
				int val = frames.Count > 0 ? frames[frames.Count - 1] : 1;
				while ( frames.Count < sprites.Count )
				{
					frames.Add( val );
				}
			}
			
			return false;
		}
		#endif
		
		//
		
		public void Reset ()
		{
			currentSprite = 0;
			currentFrame = 0;
			_animationEnded = false;
		}
		public void SetLast ()
		{
			currentSprite = sprites.Count - 1;
		}
		public void LightCopyFrom ( Anim source )
		{
			spriteList = source.spriteList;
			frames = source.frames;
			sortingLayerName = source.sortingLayerName;
			behaviour = source.behaviour;
			Reset();
		}
		public void SetBehaviour ( Behaviour be )
		{
			behaviour = be;
		}
		public void SetDirection ( Vector2 dir )
		{
			foreach ( AnimDirection adir in animDirections )
			{
				if ( dir == adir.direction )
				{
					currentAnimDirection = adir;
					return;
				}
			}
			
			// fallback
			
			currentAnimDirection = this;
		}
		
		private void TryCallback ()
		{
			if ( powerCallback != null )
			{
				if ( powerCallback() )
				{
					// ha fallito l'evocazione, eventualmente riproverà
				}
				else
				{
					powerCallback = null;
				}
			}
		}
	}
}
