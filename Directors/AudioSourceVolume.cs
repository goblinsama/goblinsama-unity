﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama
{
	[RequireComponent(typeof(AudioSource))]
	public class AudioSourceVolume : MonoBehaviour
	{
		[SerializeField] private bool playOnAwake;
		[SerializeField] private bool isMusic;
		[SerializeField][Range(0f,1f)] private float volume = 1f;
		
		private AudioSource _audioSource;
		private AudioSource audioSource {
			get {
				if (_audioSource == null)
					_audioSource = GetComponent<AudioSource>();
				return _audioSource;
			}
		}
		
		void Awake ()
		{
			if (NetworkDirector.amJustServer)
			{
				Destroy(this);
				Destroy(audioSource);
			}
		}
		void OnEnable ()
		{
			UpdateVolume();
			if (playOnAwake)
				audioSource.Play();
		}
		
		public void UpdateVolume ()
		{
			float vol = isMusic ? Director.volumeManager.music : Director.volumeManager.sound;
			audioSource.volume = vol * volume;
			audioSource.spatialBlend = isMusic ? 0f : 1f;
		}
		public void Mute ()
		{
			audioSource.volume = 0;
		}
	}
}
