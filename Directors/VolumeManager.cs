﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama
{
	public class VolumeManager : MonoBehaviourE
	{
		private float _sound;
		public float sound {
			get {
				return _sound;
			}
			set {
				_sound = value;
				UpdateVolumes();
			}
		}
		private float _music;
		public float music {
			get {
				return _music;
			}
			set {
				_music = value;
				UpdateVolumes();
			}
		}
		
		protected override void OnEnable ()
		{
			base.OnEnable();
			
			sound = PlayerPrefs.GetFloat( "soundVolume", 1f );
			music = PlayerPrefs.GetFloat( "musicVolume", 1f );
		}
		protected override bool OnDisable ()
		{
			Save(); // NOTA: deve farlo anche se è in fase di quit
			return base.OnDisable();
		}
		
		public void UpdateVolumes ()
		{
			foreach ( AudioSourceVolume asv in FindObjectsOfType<AudioSourceVolume>() )
			{
				asv.UpdateVolume();
			}
		}
		public void Mute ()
		{
			foreach ( AudioSourceVolume asv in FindObjectsOfType<AudioSourceVolume>() )
			{
				asv.Mute();
			}
		}
		public void Save ()
		{
			//Log.Add(this+" saving volume");
			
			PlayerPrefs.SetFloat( "soundVolume", sound );
			PlayerPrefs.SetFloat( "musicVolume", music );
			PlayerPrefs.Save();
		}
	}
}
