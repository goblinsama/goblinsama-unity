﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using UnityEngine.Networking;

namespace Goblinsama
{
	public class NetworkDirector : NetworkManager
	{
		public static bool amServer {
			get {
				return NetworkServer.active;
			}
		}
		public static bool amClient {
			get {
				return NetworkClient.active;
			}
		}
		public static bool amLocal {
			get {
				return !amServer && !amClient;
			}
		}
		public static bool amHost {
			get {
				return amServer && amClient;
			}
		}
		public static bool amJustServer {
			get {
				return amServer && !amHost;
			}
		}
		public static bool amJustClient {
			get {
				return amClient && !amHost;
			}
		}
		
		public static T FindNetId<T> (NetworkInstanceId id) where T:NetworkBehaviour
		{
			return System.Array.Find( FindObjectsOfType<T>(), (T check) => check.netId==id );
		}
	}
}
