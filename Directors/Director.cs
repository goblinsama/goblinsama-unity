﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

#if UNITY_EDITOR
using UnityEditor;
#endif

namespace Goblinsama
{
	[ExecuteInEditMode]
	public class Director : MonoBehaviourE
	{
		/* ****** ###### COPIARE NELLE CLASSI FIGLIE ###### ******
		new private static Director instance {
			get {
				return (Director)Goblinsama.Director.instance;
			}
		}
		*/
		
		protected static Director _instance;
		protected static Director instance {
			get {
				if (_instance == null)
					_instance = FindObjectOfType<Director>();
				return _instance;
			}
		}
		
		public static bool isHeadless {
			get {
				// non esiste un check "isHeadless" e i workaround proposti fanno schifo o non funzionano (o entrambe le cose)
				return NetworkDirector.amJustServer && Application.platform==RuntimePlatform.LinuxPlayer;
			}
		}
		
		[SerializeField] private string GAME_NAME;
		[SerializeField] private string version;
		
		/// <summary>
		/// Settato automaticamente, in genere è meglio non modificarlo.
		/// </summary>
		[SerializeField] private int incremental;
		
		[SerializeField] private string clearLogButton = "";
		
		private string lastVersionMangled;
		public static string lastVersion {
			get {
				return instance.lastVersionMangled;
			}
		}
		public static string versionFull {
			get {
				return instance.version;
			}
		}
		
		// NOTA: servono se deve accedervi da una callback
		public static float time { get; private set; }
		public static int frameCount { get; private set; }
		
		// Find*
		
		protected InstancedElements _ie;
		protected VolumeManager _volumeManager;
		protected Camera _mainCamera;
		
		// static getters
		
		public static InstancedElements ie {
			get {
				if (instance._ie == null)
					instance._ie = FindObjectOfType<InstancedElements>();
				return instance._ie;
			}
		}
		public static VolumeManager volumeManager {
			get {
				if (instance._volumeManager == null)
					instance._volumeManager = FindObjectOfType<VolumeManager>();
				return instance._volumeManager;
			}
		}
		public static Camera mainCamera {
			get {
				if ( instance._mainCamera == null )
					instance._mainCamera = Camera.main;
				return instance._mainCamera;
			}
		}
		
		// status getters
		
		public static bool active {
			get {
				return instance != null;
			}
		}
		
		// Unity
		
		protected override void Awake ()
		{
			if (_instance==null)
			{
				_instance = this;
			}
			else if (_instance!=this)
			{
				throw new UnityException("Shouldn't have two Directors.");
			}

			base.Awake();
			
			lastVersionMangled = Functions.MangleVersion(version);
			ExtraAwake();
		}
		protected override bool Update ()
		{
			if (base.Update())
				return true;
			
			time = Time.time;
			frameCount = Time.frameCount;
			
			if (Application.isPlaying && clearLogButton.Length>0)
			{
				if (Input.GetButtonDown(clearLogButton))
				{
					Log.Clear();
				}
			}
			
			#if UNITY_EDITOR
			if (!Application.isPlaying && lastIncr!=incremental)
			{
				lastIncr=incremental;
				
				UpdatePlayerSettings();
			}
			#endif
			
			return false;
		}
		
		#if UNITY_EDITOR
		
		private string lastVersionPlain;
		private int lastIncr;
		
		protected virtual void ExtraAwake ()
		{
			lastVersionPlain = version;
			lastIncr = incremental;
		}
		
		public void Check ()
		{
			if (lastVersionMangled != Functions.MangleVersion(version))
			{
				lastIncr = ++incremental;
				lastVersionMangled = Functions.MangleVersion(version);
				lastVersionPlain = version;
				
				UpdatePlayerSettings();
			}
			else if (lastVersionPlain != version)
			{
				lastVersionPlain = version;
				
				UpdatePlayerSettings();
			}
		}
		
		private void UpdatePlayerSettings ()
		{
			PlayerSettings.bundleVersion = version;
			PlayerSettings.Android.bundleVersionCode = incremental;
			PlayerSettings.iOS.buildNumber = ""+incremental;
			
			Debug.Log(string.Format("{0}: version update to {2}:[{1}]",this,version,incremental));
		}
		
		#else
		
		protected virtual void ExtraAwake () {}
		
		#endif
		
		// Statiche
		
		public static string GetVersion ()
		{
			if ( !active )
				return "";
			
			return instance.version+":"+instance.incremental;
		}
		public static string GetGameName ()
		{
			if ( !active )
				return "";
			
			return instance.GAME_NAME;
		}
	}
}
