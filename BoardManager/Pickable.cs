﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.BoardManager
{
	/// <summary>
	/// An object that can stay in a BoardCell and can be picked up by an entity.
	/// </summary>
	public abstract class Pickable : MonoBehaviourE
	{
		/// <summary>
		/// Applies the Pickable to the specified target.
		/// 
		/// Return paradygm:
		///  This should be overridden and base called, checking its return value.
		///  In case it's true, you should immediately return true.
		///  In case it's false, you should proceed and then return false if everything went ok,
		///  and return true in case you ended up doing nothing instead.
		/// </summary>
		public virtual bool PickUp (GameObject target) { return false; }
		
		protected override bool OnTriggerEnter2D (Collider2D col)
		{
			if (base.OnTriggerEnter2D(col))
				return true;
			
			if (PickUp(col.gameObject))
				return true;
			
			return false;
		}
	}
}
