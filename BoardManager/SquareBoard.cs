﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System;

namespace Goblinsama.BoardManager
{
	public class SquareBoard : Board
	{
		protected override Vector3 LinePosition (ref float iy)
		{
			Vector3 lpos = Vector3.zero;
			lpos.x = 0f;
			lpos.y = iy;
			iy += cellHeight;
			return lpos;
		}
		
		public override Vector2 IndexToPosition (int ix, int iy)
		{
			Vector2 res = Vector2.zero;

			res.x = ix * cellWidth;
			res.y = iy * cellHeight;

			return res;
		}
		
		public override Vector2 PositionToIndex (float xpos, float ypos)
		{
			Vector2 res = Vector2.zero;

			int yDiv = (int)(Math.Round(ypos / cellHeight));
			float yRem = ypos % cellHeight;
			if ( yRem < -cellHeight * .5f )
				yDiv--;

			int xDiv = (int)(Math.Round(xpos / cellWidth));
			float xRem = xpos % cellWidth;
			if ( xRem < -cellWidth * .5f )
				xDiv--;

			res.x = xDiv;
			res.y = yDiv;

			return res;
		}
	}
}
