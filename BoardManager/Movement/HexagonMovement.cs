﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using System;
using UnityEngine;

namespace Goblinsama.BoardManager
{
	/// <summary>
	/// Manages the movement on the hex cell board.
	/// </summary>
	public class HexagonMovement : BoardMovement
	{
		protected override void Awake ()
		{
			base.Awake();
			board = GetComponent<HexagonBoard>();
		}
		
		protected override void ProcessGo (CellInfos xy, int incX, int incY)
		{
			int ix = xy.x;
			int iy = xy.y;
			
			if (incY == 0)
			{
				ix += incX;
			}
			else
			{
				iy += incY;
				if (incX > 0)
					ix = (iy % 2 == 0) ? (ix + incX) : ix;
				else
					ix = (iy % 2 == 0) ? ix : (ix + incX);
			}
			
			xy.Set(ix,iy);
		}
	}
}
