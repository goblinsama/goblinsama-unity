﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.BoardManager
{
	public class CellInfos
	{
		public int x {
			get {
				return _x;
			}
		}
		public int y {
			get {
				return _y;
			}
		}
		public int safeX {
			get {
				if (saved==null)
					saved = new CellInfoSave(this);
				return saved.x;
			}
		}
		public int safeY {
			get {
				if (saved==null)
					saved = new CellInfoSave(this);
				return saved.y;
			}
		}
		
		protected int _x,_y;
		private CellInfoSave saved;
		
		public CellInfos ()
		{
			Set(0,0);
		}
		
		public void Set (int toX, int toY)
		{
			_x = toX;
			_y = toY;
		}
		public void Set (Vector2 to)
		{
			Set( (int)to.x, (int)to.y );
		}
		public void Incr (int incX, int incY)
		{
			Set( _x+incX, _y+incY );
		}
		
		public void SaveInternal ()
		{
			saved = new CellInfoSave(this);
		}
		public void SaveInternal (int x, int y)
		{
			saved = new CellInfoSave(x,y);
		}
		public void SaveInternal (Vector2 xy)
		{
			SaveInternal( (int)xy.x, (int)xy.y );
		}
		public void Back ()
		{
			_x = saved.x;
			_y = saved.y;
		}
		
		public CellInfoSave SaveReturn ()
		{
			return new CellInfoSave(this);
		}
		public void Load (CellInfoSave from)
		{
			saved = from;
			Back();
		}
	}
}
