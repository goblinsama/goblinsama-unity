﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.BoardManager
{
	/// <summary>
	/// Salvataggio di una CellInfo.
	/// </summary>
	public class CellInfoSave
	{
		private int _x,_y;
		
		public int x {
			get {
				return _x;
			}
		}
		public int y {
			get {
				return _y;
			}
		}
		
		public CellInfoSave (CellInfos source)
		{
			Set(source);
		}
		public CellInfoSave (int x, int y)
		{
			Set(x,y);
		}
		public void Set (CellInfos source)
		{
			Set(source.x,source.y);
		}
		public void Set (int x, int y)
		{
			_x = x;
			_y = y;
		}
	}
}
