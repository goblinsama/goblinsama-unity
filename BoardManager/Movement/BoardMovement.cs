﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.BoardManager
{
	public abstract class BoardMovement : MonoBehaviourE
	{
		protected Board board;
		
		[SerializeField] protected Vector3 offset;
		
		/// <summary>
		/// Gets the world position for the target cell.
		/// </summary>
		/// <returns>The world position, modified by the offset.</returns>
		public virtual Vector3 GetFinalPosition (int ix, int iy)
		{
			BoardCell bc = board.GetCellXY(ix, iy);
			return bc.transform.position + offset;
		}
		public Vector3 GetFinalPosition (CellInfos xy)
		{
			return GetFinalPosition(xy.x,xy.y);
		}
		
		/// <summary>
		/// This function updates the XY indexes, based on the given direction.
		/// </summary>
		/// <returns>True if the operation succeeds, false otherwise.</returns>
		private bool Go (CellInfos xy, int incX, int incY)
		{
			CellInfoSave savedCell = xy.SaveReturn();
			try
			{
				ProcessGo(xy,incX,incY);
			}
			catch (System.ArgumentOutOfRangeException)
			{
				// TODO-; così non va bene, perché annulla per intero un movimento parzialmente valido
				xy.Load(savedCell);
				return false;
			}
			
			return true;
		}
		
		protected abstract void ProcessGo (CellInfos xy, int incX, int incY);
		
		/// <summary>
		/// Updates the XY indexes, based on the given direction, for the amount of given steps.
		/// </summary>
		protected virtual void GoStraight (CellInfos xy, int dirX, int dirY, int steps)
		{
			for (int step = 0; step < steps; step++)
			{
				if (!Go(xy, dirX, dirY))
					return;
			}
		}
		
		/// <summary>
		/// Data una cella XY, una direzione di spostamento, e quante volte mi devo spostare, restituisce la posizione in coordinate nel mondo.
		/// </summary>
		/// <param name="ix">l'indice X della cella.</param>
		/// <param name="iy">l'indice Y della cella.</param>
		/// <param name="dir">la direzione di spostamento XY.</param>
		/// <param name="step">quante volte spostarsi nella direzione data.</param>
		/// <returns>la posizione in coordinate nel mondo.</returns>
		public Vector3 Move (CellInfos xy, Vector2 dir, int step)
		{
			GoStraight(xy, (int)dir.x, (int)dir.y, step);
			return GetFinalPosition(xy);
		}
	}
}
