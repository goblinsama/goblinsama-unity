﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections.Generic;
using System;

namespace Goblinsama.BoardManager
{
	/// <summary>
	/// This class is the base from which are derived the other boards
	/// </summary>
	[ExecuteInEditMode]
	public abstract class Board : Goblinsama.MonoBehaviourE
	{
		#if UNITY_EDITOR
		
		/// <summary>
		/// Usato da CellPainter.
		/// </summary>
		public BoardCell[] cellTypes;
		
		[SerializeField] protected bool DELETE;
		[SerializeField] protected int createW;
		[SerializeField] protected int createH;
		[SerializeField] protected bool CREATE;
		
		#endif
		
		/// <summary>
		/// Cell width and height.
		/// </summary>
		[SerializeField] protected float cellWidth;
		[SerializeField] protected float cellHeight;
		
		/// <summary>
		/// The cells (Row, Col).
		/// </summary>
		[SerializeField] protected List<List<BoardCell>> cells;
		
		private List<Vector2> startPositions;
		private Stack<Vector2> availableStartPositions;
		
		protected override void OnEnable ()
		{
			base.OnEnable();
			
			if (cells == null)
			{
				cells = new List<List<BoardCell>>();
				Populate();
			}
			
			Recalc();
			
			if (Application.isPlaying)
			{
				// trova le starting position
				startPositions = new List<Vector2>();
				for (int lineY=0; lineY<cells.Count; lineY++)
				{
					List<BoardCell> line = cells[lineY];
					
					for (int colX=0; colX<line.Count; colX++)
					{
						BoardCell cell = line[colX];
						if (cell is StartingCell)
						{
							startPositions.Add(new Vector2(colX,lineY));
						}
					}
				}
				
				availableStartPositions = new Stack<Vector2>(startPositions);
			}
		}
		#if UNITY_EDITOR
		protected override bool Update ()
		{
			if (base.Update())
				return true;
			
			if (Application.isPlaying)
				return false;
			
			if (DELETE)
			{
				Delete();
				DELETE = false;
				return true;
			}
			
			if (CREATE)
			{
				Create(createW,createH);
				CREATE = false;
				return true;
			}
			
			Recalc();
			
			return false;
		}
		#endif
		
		protected virtual void Recalc ()
		{
			float iy = 0;
			
			foreach (Transform tr in transform)
			{
				BoardLine line = tr.GetComponent<BoardLine>();
				if (!line)
					continue;
				
				line.transform.localPosition = LinePosition(ref iy);
				
				// cells positon in the line
				float ix = 0;
				foreach (Transform hx in line.transform)
				{
					BoardCell cell = hx.GetComponent<BoardCell>();
					if (!cell)
						continue;
					
					Vector3 hpos = Vector3.zero;
					hpos.x = ix;
					hpos.y = 0;
					hx.localPosition = hpos;
					ix += cellWidth;
					
					cell.x = ((int)Math.Round(ix / cellWidth) - 1);
					cell.y = ((int)Math.Round(iy / cellHeight) - 1);
				}
			}
		}
		protected virtual void Populate ()
		{
			foreach (Transform tr in transform)
			{
				BoardLine line = tr.GetComponent<BoardLine>();
				if (!line)
					continue;
				
				List<BoardCell> cellsLine = new List<BoardCell>();
				foreach (Transform cell in line.transform)
				{
					BoardCell bc = cell.GetComponent<BoardCell>() as BoardCell;
					cellsLine.Add(bc);
				}
				
				cells.Add(cellsLine);
			}
		}
		#if UNITY_EDITOR
		protected virtual void Delete ()
		{
			List<GameObject> toDestroy = new List<GameObject>();
			
			foreach (Transform child in transform)
				toDestroy.Add(child.gameObject);
			
			foreach (GameObject des in toDestroy)
				DestroyImmediate(des);
			
			cells.Clear();
			startPositions = null;
		}
		protected virtual void Create (int cw, int ch)
		{
			for (int iy=0; iy<ch; iy++)
			{
				GameObject line = new GameObject();
				line.name = "Board line "+iy;
				line.transform.SetParent(transform,false);
				line.AddComponent<BoardLine>();
				
				for (int ix=0; ix<cw; ix++)
				{
					GameObject ncel = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab(cellTypes[0].gameObject);
					ncel.name = "Cell "+iy+"."+ix+"";
					ncel.transform.SetParent(line.transform,false);
				}
			}
			
			Populate();
			Recalc();
		}
		#endif
		
		protected abstract Vector3 LinePosition (ref float iy);
		
		public Vector2 GetStartPosition ()
		{
			return availableStartPositions.Pop();
		}
		public bool StartingSlotsAvailable ()
		{
			return availableStartPositions.Count>0;
		}
		
		/// <summary>
		/// Is there a cell in the given position?
		/// </summary>
		/// <returns>True if the cell exists, false otherwise.</returns>
		public virtual bool HasCellXY (int ix, int iy)
		{
			int lineIndex = iy;
			int cellIndex = ix;
			
			if (lineIndex < 0 || lineIndex >= cells.Count)
				return false;
			
			List<BoardCell> cl = cells[lineIndex];
			if (cellIndex < 0 || cellIndex >= cl.Count)
				return false;
			
			return true;
		}
		
		/// <summary>
		/// Gets the cell in the given position.
		/// </summary>
		/// <returns>The target cell.</returns>
		public virtual BoardCell GetCellXY (int ix, int iy)
		{
			List<BoardCell> cl = cells[iy];
			BoardCell bc = cl[ix];
			return bc;
		}
		
		/*
		/// <summary>
		/// Gets and applies a Pickable from the target cell.
		/// </summary>
		/// <returns>Returns the Pickable, if any, null otherwise.</returns>
		public virtual Pickable PickupCell (GameObject picker, int ix, int iy)
		{
			Pickable pick = GetCellXY(ix,iy).GetPickable();
			if (pick == null)
				return null;
			
			pick.Apply(picker);
			return pick;
		}
		*/
		
		public abstract Vector2 PositionToIndex (float xpos, float ypos);
		public abstract Vector2 IndexToPosition (int ix, int iy);
		public Vector2 PositionToIndex (Vector2 pos)
		{
			return PositionToIndex(pos.x,pos.y);
		}
	}
}
