/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections.Generic;

namespace Goblinsama.BoardManager
{
	public static class CellPainter
	{
		#if UNITY_EDITOR
		
		// TODO-. avere un singolo comando e fare la selezione prima da Board, con varie opzioni eventuali
		[UnityEditor.MenuItem("Goblinsama/Board Manager/Paint with cell 0")]
		public static void PaintWithCell0 ()
		{
			PaintWithCellX(0);
		}

		[UnityEditor.MenuItem("Goblinsama/Board Manager/Paint with cell 1")]
		public static void PaintWithCell1 ()
		{
			PaintWithCellX(1);
		}

		[UnityEditor.MenuItem("Goblinsama/Board Manager/Paint with cell 2")]
		public static void PaintWithCell2 ()
		{
			PaintWithCellX(2);
		}

		private static void PaintWithCellX (int id)
		{
			Board board = MonoBehaviour.FindObjectOfType<Board>();
			if (board == null)
			{
				Debug.LogError("Couldn't find Board");
				return;
			}
			
			BoardCell prefab;
			if ( (id>=board.cellTypes.Length) || ((prefab=board.cellTypes[id])==null) )
			{
				Debug.LogError("Board cell type <"+id+"> not set.");
				return;
			}
			
			List<Transform> trs = new List<Transform>(UnityEditor.Selection.transforms);
			foreach (Transform tr in trs)
			{
				BoardCell cel = tr.GetComponent<BoardCell>();
				if (!cel)
					continue;
				
				// crea la nuova cella e la mette al posto giusto
				GameObject ncel = (GameObject)UnityEditor.PrefabUtility.InstantiatePrefab(prefab.gameObject);
				ncel.transform.SetParent(tr.parent);
				ncel.transform.SetSiblingIndex( tr.GetSiblingIndex() );
				// TODO-. ncel.name = cel.name;
				
				// distrugge la vecchia
				GameObject.DestroyImmediate(cel.gameObject);
				
				// segna la scena come modificata
				UnityEditor.SceneManagement.EditorSceneManager.MarkSceneDirty( ncel.scene );
			}
		}
		
		#endif
	}
}
