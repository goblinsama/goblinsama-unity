# License #

This library is distributed under the Microsoft Public License "MS-PL".

For details see [LICENSE](/LICENSE) file.

# Authors #

This library is brought to you by [Goblinsama Ltd.](https://goblinsama.com)

The authors disclaim warranties and guarantees: use this at your own risk, or not at all.
