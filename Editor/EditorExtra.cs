﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

#if UNITY_EDITOR

using UnityEngine;
using System.Collections;
using UnityEditor;

namespace Goblinsama
{
	public static class EditorExtra
	{
		[UnityEditor.MenuItem( "Goblinsama/Center Box" )]
		public static void CenterBox ()
		{
			BoxCollider box = GetSingleSelection<BoxCollider>();
			if ( !box )
				return;
			
			box.transform.position += box.transform.rotation * box.center;
			box.center = Vector3.zero;
		}
		
		[UnityEditor.MenuItem("Goblinsama/Snap position H")]
		public static void SnapPositionH ()
		{
			Snap.SnapPosition(true,false);
		}
		[UnityEditor.MenuItem("Goblinsama/Snap position HV")]
		public static void SnapPositionHV ()
		{
			Snap.SnapPosition(true,true);
		}
		
		[UnityEditor.MenuItem("Goblinsama/Revert to prefab")]
		public static void Revert ()
		{
			foreach (GameObject go in Selection.gameObjects)
			{
				Undo.RecordObject(go, "Revert to prefab");
				PrefabUtility.RevertPrefabInstance(go);
			}
		}
		
		private static T GetSingleSelection<T> ()
		{
			T sel = default(T);
			if ( Selection.gameObjects.Length != 1 || ( sel = Selection.gameObjects[ 0 ].GetComponent<T>() ) == null )
			{
				Debug.LogWarning( "You have to select one (1) target " + typeof( T ).Name + "." );
				return sel; // NOTA: avrebbe voluto essere un return null.
			}
			return sel;
		}
	}
}

#endif
