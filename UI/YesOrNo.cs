﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;

namespace Goblinsama.UI
{
	public class YesOrNo : MonoBehaviour
	{
		[SerializeField] private Text question;
		[SerializeField] private Button yesButton;
		[SerializeField] private Button noButton;
		
		private System.Action actionYes;
		private System.Action actionNo;
		
		public void SetupImages ( string questionText, System.Action onYes, System.Action onNo )
		{
			question.text = questionText;
			SetCallbacks( onYes, onNo );
			ActivateChildTI( yesButton, true );
			ActivateChildTI( noButton, true );
			gameObject.SetActive( true );
		}
		public void SetupText ( string questionText, System.Action onYes, System.Action onNo, string textYes, string textNo )
		{
			question.text = questionText;
			SetCallbacks( onYes, onNo );
			ActivateChildTI( yesButton, false, textYes );
			ActivateChildTI( noButton, false, textNo );
			gameObject.SetActive( true );
		}
		public void ButtonYesNo ( bool yes )
		{
			if ( yes )
				actionYes();
			else
				actionNo();
			gameObject.SetActive( false );
		}
		
		private void ActivateChildTI ( Selectable parent, bool activateImage, string text = null )
		{
			foreach ( Transform child in parent.transform )
			{
				Image img = child.GetComponent<Image>();
				if ( img )
				{
					img.enabled = activateImage;
				}
				
				Text txt = child.GetComponent<Text>();
				if ( txt )
				{
					txt.enabled = !activateImage;
					if ( text != null )
						txt.text = text;
				}
			}
		}
		private void SetCallbacks ( System.Action onYes, System.Action onNo )
		{
			actionYes = onYes;
			actionNo = onNo;
		}
	}
}
