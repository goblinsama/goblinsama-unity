﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Goblinsama.UI
{
	[ExecuteInEditMode]
	public abstract class VHList : MonoBehaviourE
	{
		[SerializeField] protected float _padding;
		public float padding {
			get {
				return _padding;
			}
		}
		
		public bool forceRefresh = false;
		
		protected List<VHElement> children = new List<VHElement>();
		
		protected int childrenCount = 0;
		
		protected override bool Update ()
		{
			if ( base.Update() )
				return true;
			
			if ( forceRefresh || ( childrenCount != transform.childCount ) )
			{
				forceRefresh = false;
				childrenCount = transform.childCount;
				
				Refresh();
			}
			
			return false;
		}
		
		protected abstract bool IsVertical ();
		
		public void DelayedRefresh ()
		{
			forceRefresh = true;
		}
		public void Refresh ()
		{
			children.Clear();
			
			float totalEl = 0;
			
			foreach ( Transform tr in transform )
			{
				VHElement el = tr.GetComponent<VHElement>();
				
				if (el)
				{
					// NOTA: si mette il ratio a 0 per ignorare questo elemento
					if ( el.ratio <= 0 )
						continue;
				}
				else
				{
					el = tr.gameObject.AddComponent<VHElement>();
					el.ratio = 1;
				}
				
				children.Add( el );
				
				totalEl += el.ratio;
			}
			
			float perc = 1f / totalEl;
			float currentVal = IsVertical() ? 1 : 0;
			Vector2 vec2 = new Vector2();
			
			foreach ( VHElement child in children )
			{
				if ( IsVertical() )
				{
					vec2.Set( 1, currentVal );
					child.rectTransform.anchorMax = vec2;
					
					currentVal -= perc * child.ratio;
					
					vec2.Set( 0, currentVal );
					child.rectTransform.anchorMin = vec2;
					
					vec2.Set( 0, padding / 2 );
				}
				else
				{
					vec2.Set( currentVal, 0 );
					child.rectTransform.anchorMin = vec2;
					
					currentVal += perc * child.ratio;
					
					vec2.Set( currentVal, 1 );
					child.rectTransform.anchorMax = vec2;
					
					vec2.Set( padding / 2, 0 );
				}
				
				child.rectTransform.offsetMax = -vec2;
				child.rectTransform.offsetMin = vec2;
			}
		}
	}
}
