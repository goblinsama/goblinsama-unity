/*
	© 2017 Goblinsama Ltd., All rights reserved
 */

using UnityEngine;

namespace Goblinsama.UI
{
	public class VHElement : MonoBehaviour
	{
		public float ratio = 1;
		
		private RectTransform _rectTransform;
		public RectTransform rectTransform {
			get {
				if ( _rectTransform == null )
					_rectTransform = GetComponent<RectTransform>();
				return _rectTransform;
			}
		}
	}
}
