﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Goblinsama
{
	public class Log : MonoBehaviourE
	{
		[SerializeField] private string warningPrefix = "***W*** ";
		[SerializeField] private string errorPrefix = "###E### ";
		[SerializeField] private Color warningColor = new Color32(255,255,0,255);
		[SerializeField] private Color errorColor = new Color32(255,0,0,255);
		[SerializeField] private short lineMax = 10; // TODO. renderlo automatico in base alla grandezza del testo
		
		private Text textUI;
		
		private static string text = "";
		private static List<string> lines = new List<string>();
		
		private static string warningPrefixS;
		private static string errorPrefixS;
		private static Color warningColorS;
		private static Color errorColorS;
		private static short lineMaxS;
		private static bool debugging = false;
		
		protected override void Awake ()
		{
			base.Awake();
			textUI = GetComponent<Text>();
		}
		protected override void OnEnable ()
		{
			base.OnEnable();
			
			warningPrefixS = warningPrefix;
			errorPrefixS = errorPrefix;
			warningColorS = warningColor;
			errorColorS = errorColor;
			lineMaxS = lineMax;
			
			if ( !Debug.isDebugBuild )
			{
				gameObject.SetActive( false );
			}
		}
		protected override bool Update ()
		{
			if ( base.Update() )
				return true;
			
			textUI.text = text;
			
			return false;
		}
		
		public static void Set ( string txt, string prefix="" )
		{
			Clear();
			Add( txt, prefix );
		}
		public static void Add ( string txt, string prefix="" )
		{
			Add( txt, prefix, Color.clear );
		}
		public static void Add ( string txt, string prefix, Color32 color )
		{
			if ( Application.isEditor || debugging )
			{
				Debug.Log( string.Format( "<{1}> {2}{0}", txt, System.DateTime.UtcNow, prefix ) );
			}
			
			if ( Director.isHeadless )
			{
				return;
			}
			
			if ( color != Color.clear )
				txt = string.Format( "<color=#{1:X2}{2:X2}{3:X2}ff>{0}</color>", txt, color.r, color.g, color.b );
			
			lines.Add( txt );
			while ( lineMaxS > 0 && lines.Count > lineMaxS )
			{
				lines.RemoveAt( 0 );
			}
			text = "";
			foreach ( string st in lines )
			{
				if ( text != "" )
					text += "\n";
				text += st;
			}
		}
		public static void AddWarning ( string txt )
		{
			Add( txt, warningPrefixS, warningColorS );
		}
		public static void AddError ( string txt )
		{
			Add( txt, errorPrefixS, errorColorS );
		}
		
		public static void Clear ()
		{
			text = "";
		}
		public static void Debugging ( bool active )
		{
			debugging = active;
			Add( "Debug mode " + ( debugging ? "activated" : "disabled" ) + "" );
		}
	}
}
