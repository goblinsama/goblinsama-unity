/*
	© 2020 Goblinsama Ltd., All rights reserved
 */

using Goblinsama;
using UnityEngine;
using UnityEngine.UI;

namespace Pixels_of_prey
{
	[RequireComponent(typeof(Text))]
	public class TextFromFiles : MonoBehaviourE
	{
		[SerializeField] private TextAsset[] files;

		protected override void Start ()
		{
			base.Start();

			Text target = GetComponent<Text>();
			target.text = "";

			foreach ( TextAsset ta in files )
			{
				if ( target.text.Length>0 ) target.text += "\n\n";

				target.text += ta.text;
			}
		}
	}
}
