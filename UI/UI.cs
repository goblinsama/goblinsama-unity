﻿/*  Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.UI
{
	public class UI : MonoBehaviourE
	{
		public MenuSwitcher menu;
		
		private RectTransform showOnSuccess;
		private RectTransform showOnFailure;
		
		public void OnSuccess ( RectTransform target )
		{
			showOnSuccess = target;
		}
		public void OnFailure ( RectTransform target )
		{
			showOnFailure = target;
		}
		
		public void Success ()
		{
			menu.Show( showOnSuccess );
			ResetSF();
		}
		public void Failure ()
		{
			menu.Show( showOnFailure );
			ResetSF();
		}
		
		private void ResetSF ()
		{
			showOnSuccess = null;
			showOnFailure = null;
		}
	}
}
