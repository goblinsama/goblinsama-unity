﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;

namespace Goblinsama.UI
{
	[ExecuteInEditMode]
	public class KeepProportions : MonoBehaviourQ
	{
		/// <summary>
		/// La dimensione originale.
		/// </summary>
		[SerializeField] private Vector2 originalSize;
		
		/// <summary>
		/// La dimensione di schermo a cui originalSize è rapportata.
		/// </summary>
		[SerializeField] private Vector2 screenReference;
		
		/// <summary>
		/// Se basarsi sull'altezza attuale (true), o sulla larghezza (false).
		/// </summary>
		[SerializeField] private bool trustHeight = false;
		
		/// <summary>
		/// L'ultima risoluzione usata per calcolare il valore attuale.
		/// </summary>
		private Vector2 lastResolution = Vector2.zero;
		
		void Update ()
		{
			if ( lastResolution.x != Screen.width || lastResolution.y != Screen.height )
			{
				// la risoluzione è cambiata, dunque ricalcola
				
				float curTrust;
				float origRef;
				if ( trustHeight )
				{
					curTrust = Screen.height;
					origRef = screenReference.y;
				}
				else
				{
					curTrust = Screen.width;
					origRef = screenReference.x;
				}
				
				float proportion = curTrust / origRef;
				Vector2 result = originalSize * proportion;
				
				// applica
				
				rectTransform.sizeDelta = result;
				
				// salva
				
				lastResolution.x = Screen.width;
				lastResolution.y = Screen.height;
			}
		}
	}
}
