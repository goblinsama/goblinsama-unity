﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.UI
{
	public class LogShow : MonoBehaviour
	{
		[SerializeField] private bool autoShowLog;
		[SerializeField] private GameObject[] showThese;
		
		void Start ()
		{
			foreach (GameObject go in showThese)
			{
				go.SetActive(true);
			}
			
			if (autoShowLog) foreach (Transform tr in transform)
			{
				Log log = tr.GetComponent<Log>();
				if (!log)
					continue;
				
				log.gameObject.SetActive( Debug.isDebugBuild );
			}
		}
	}
}
