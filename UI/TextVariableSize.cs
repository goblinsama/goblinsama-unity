﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;

namespace Goblinsama.UI
{
	[ExecuteInEditMode]
	[RequireComponent(typeof(Text))]
	public class TextVariableSize : MonoBehaviour
	{
		[SerializeField] private int fontSize;
		[SerializeField] private int resolutionReference = 1080;
		
		private Text _textUI;
		private Text textUI {
			get {
				if ( _textUI == null )
					_textUI = GetComponent<Text>();
				return _textUI;
			}
		}
		
		void Update ()
		{
			if ( fontSize == 0 )
			{
				fontSize = textUI.fontSize;
			}
			if ( resolutionReference == 0 )
			{
				resolutionReference = Screen.height;
			}
			
			textUI.resizeTextForBestFit = false;
			textUI.fontSize = fontSize * Screen.height / resolutionReference;
		}
		
		public void SetFontSize (int size)
		{
			fontSize = size;
		}
	}
}
