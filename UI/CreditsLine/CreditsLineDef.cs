﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;

namespace Goblinsama.UI
{
	public class CreditsLineDef : MonoBehaviour
	{
		[SerializeField][Multiline] private string title;
		[SerializeField][Multiline] private string value;
		
		[SerializeField] private float heightRatio = 1;
		
		[SerializeField] private CreditsLineInt interfacePrefab;
		[HideInInspector] public CreditsLineInt interfaceInstanced;
		
		public void WakeUp ()
		{
			// init
			
			interfaceInstanced = Instantiate(interfacePrefab);
			
			VHElement el = interfaceInstanced.gameObject.AddComponent<VHElement>();
			el.ratio = heightRatio;
			
			interfaceInstanced.transform.SetParent(transform.parent);
			
			// text
			
			interfaceInstanced.title.text = title;
			interfaceInstanced.value.text = value;
			
			// color
			
			CreditsLines cLines = GetComponentInParent<CreditsLines>();
			interfaceInstanced.title.color = cLines.color;
			interfaceInstanced.value.color = cLines.color;
		}
		public void Delete ()
		{
			DestroyImmediate( interfaceInstanced.gameObject );
		}
	}
}
