﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2016-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.UI
{
	[ExecuteInEditMode]
	public class CreditsLines : MonoBehaviour
	{
		[SerializeField] private Color textColor = Color.white;
		
		#region Debug properties
		
		#if !UNITY_EDITOR
		#pragma warning disable 0414
		#endif
		[SerializeField] private bool e_WakeUp = false;
		[SerializeField] private bool e_Remove = false;
		#if !UNITY_EDITOR
		#pragma warning restore 0414
		#endif
		
		#endregion
		
		public Color color {
			get {
				return textColor;
			}
		}
		
		void Awake ()
		{
			if ( !Application.isPlaying )
				return;
			
			WakeUp();
		}
		#if UNITY_EDITOR
		void Update ()
		{
			if ( e_WakeUp )
			{
				e_WakeUp = false;
				
				WakeUp();
				
				return;
			}
			if ( e_Remove )
			{
				e_Remove = false;
				
				foreach ( CreditsLineDef cld in GetComponentsInChildren<CreditsLineDef>() )
				{
					cld.Delete();
				}
				
				return;
			}
		}
		#endif
		
		public void WakeUp ()
		{
			foreach ( CreditsLineDef cld in GetComponentsInChildren<CreditsLineDef>() )
			{
				cld.WakeUp();
			}
			GetComponent<VHList>().Refresh();
		}
	}
}
