﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;
using Goblinsama;

namespace Goblinsama.UI
{
	public class HudListItem : UpdatingItem
	{
		[SerializeField] protected Text head;
		[SerializeField] protected Text val;
		[SerializeField] protected InputField inputField;
		
		protected override void DoUpdate ()
		{
			if ( head )
			{
				Functions.UpdateEditor( head, () => {
					head.name = name + " head";
					head.text = name;
				}, "Fixing head" );
			}
			if ( val )
			{
				Functions.UpdateObject( val.gameObject, name + " value" );
			}
			if ( inputField )
			{
				Functions.UpdateObject( inputField.gameObject, name + " input" );
			}
		}
	}
}
