﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama.UI
{
	[ExecuteInEditMode]
	public abstract class UpdatingItem : MonoBehaviourE
	{
		private string didName = null;
		
		protected override void OnEnable ()
		{
			base.OnEnable();
			
			DoUpdate();
			didName = name;
		}
		protected override bool Update ()
		{
			if ( base.Update() )
				return true;
			
			if ( !ShouldUpdate() )
				return true;
			
			DoUpdate();
			didName = name;
			
			return false;
		}
		
		protected abstract void DoUpdate ();
		
		protected bool ShouldUpdate ()
		{
			if ( didName == name )
				return false;
			
			return true;
		}
	}
}
