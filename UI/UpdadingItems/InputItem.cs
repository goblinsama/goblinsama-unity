﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using UnityEngine.UI;
using System.Collections.Generic;

namespace Goblinsama.UI
{
	public class InputItem : UpdatingItem
	{
		[SerializeField] private string[] preferenceNames;
		[SerializeField] private bool autoLoadValues;
		
		private List<InputField> inputFields = new List<InputField>();
		private string showName;
		private string[] defaultValues;
		
		protected override void OnEnable ()
		{
			base.OnEnable();
			
			if ( autoLoadValues )
				LoadValues( null );
		}
		protected override void DoUpdate ()
		{
			//Debug.Log( this + ".DoUpdate" );
			
			int inputCount = 0;
			int textCount = 0;
			
			if ( preferenceNames.Length == 0 )
			{
				Functions.UpdateEditor( this, () => {
					preferenceNames = new string[1];
					preferenceNames[0] = name;
				} );
			}
			
			inputFields.Clear();
			foreach ( Transform tr in transform )
			{
				InputField input;
				Text text;
				string pre;
				
				input = tr.GetComponent<InputField>();
				if ( input )
				{
					if ( preferenceNames.Length <= inputCount )
					{
						pre = name + " input#" + inputCount;
						Debug.LogWarning( this + " missing preference name#" + inputCount );
					}
					else
					{
						pre = GetName( inputCount );
					}
					inputFields.Add( input );
					
					Functions.UpdateObject( input.gameObject, pre == name ? pre + " input" : pre );
					Functions.UpdateObject( input.textComponent.gameObject, pre + " text" );
					Functions.UpdateObject( input.placeholder.gameObject, pre + " placeholder" );
					
					inputCount++;
					
					continue;
				}
				
				text = tr.GetComponent<Text>();
				if ( text )
				{
					if ( textCount == 0 )
					{
						pre = name + " head";
					}
					else
					{
						pre = name + " text#" + textCount;
					}
					
					Functions.UpdateObject( text.gameObject, pre );
					Functions.UpdateEditor( text, () => {
						text.text = ( showName == null || showName.Length == 0 ) ? name : showName;
					} );
					
					textCount++;
					
					continue;
				}
			}
		}
		
		private string SArr ( string[] arr )
		{
			if ( arr == null )
				return "";
			string ret = "";
			foreach (string st in arr)
			{
				ret += "[" + st + "]";
			}
			return ret;
		}
		public void LoadValues ( string[] defs )
		{
			//Debug.Log( this + ".LoadValues" );
			
			if ( defs != null && defs.Length != preferenceNames.Length )
				throw new System.ArgumentException( "Defaults amount error" );
			
			//Debug.Log( string.Format( "{0}.LoadValues: current defs [{1}], new defs [{2}]", this, SArr( defaultValues ), SArr( defs ) ) );
			defaultValues = defs;
			
			GatherInputFields(); // NOTA: qua ci va sennò quando li istanzia a runtime non funziona
			//Debug.Log( this + " has " + preferenceNames.Length + " preference names and " + inputFields.Count + " input fields" );
			
			for ( int ii = 0; ii < preferenceNames.Length; ii++ )
			{
				string getName = GetName( ii );
				
				//Debug.Log( string.Format( "Getting name [{0}], has value: {1}, has defaults: {2}, saved [{3}]", getName, PlayerPrefs.HasKey( getName ), defaultValues != null, PlayerPrefs.GetString( getName ) ) );
				
				if ( defaultValues == null )
				{
					inputFields[ii].text = PlayerPrefs.GetString( getName );
					//Debug.Log( string.Format( "Getting name [{0}], has value {1}, got [{2}], no defaults", getName, PlayerPrefs.HasKey( getName ), inputFields[ii].text ) );
				}
				else
				{
					inputFields[ii].text = PlayerPrefs.GetString( getName, defaultValues[ii] );
					//Debug.Log( string.Format( "Getting name [{0}], has value: {1}, got [{2}], default: [{3}]", getName, PlayerPrefs.HasKey( getName ), inputFields[ii].text, defaultValues[ii] ) );
				}
				
				//Debug.Log( this + " loading [" + getName + "] got [" + inputFields[ii].text + "]" );
			}
		}
		public void Confirm ()
		{
			GatherInputFields();
			
			for ( int ii = 0; ii < preferenceNames.Length; ii++ )
			{
				string saveName = GetName( ii );
				string saveValue = inputFields[ii].text;
				
				//Debug.Log( this + " saving [" + saveName + "] : [" + saveValue + "]" );
				PlayerPrefs.SetString( saveName, saveValue );
			}
			
			PlayerPrefs.Save();
		}
		public string GetName ( int idx )
		{
			string pName = preferenceNames[idx];
			return name == pName ? name : name + ": " + pName;
		}
		public void HideField ( int idx )
		{
			inputFields[idx].gameObject.SetActive( false );
		}
		public void ShowName ( string text )
		{
			showName = text;
		}
		public void ResetValues ()
		{
			//Debug.Log( this + ".ResetValues" );
			
			for ( int ii = 0; ii < preferenceNames.Length; ii++ )
			{
				if ( defaultValues == null )
				{
					inputFields[ii].text = "";
				}
				else
				{
					inputFields[ii].text = defaultValues[ii];
				}
			}
		}
		
		private void GatherInputFields ()
		{
			if ( inputFields.Count < 1 )
			{
				foreach ( Transform tr in transform )
				{
					InputField inf = tr.GetComponent<InputField>();
					if ( !inf )
						continue;
					//Debug.Log( this + " found input field: " + inf );
					inputFields.Add( inf );
				}
			}
		}
	}
}
