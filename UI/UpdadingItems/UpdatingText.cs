/*
	© 2017 Goblinsama Ltd., All rights reserved
 */

using UnityEngine;
using UnityEngine.UI;

namespace Goblinsama.UI
{
	public class UpdatingText : UpdatingItem
	{
		[SerializeField] private Text target;
		
		protected override void DoUpdate ()
		{
			if ( !target )
			{
				target = GetComponentInChildren<Text>();
				if ( !target )
					return;
			}
			
			target.text = name;
			
			if ( target.gameObject != gameObject )
			{
				target.name = name + " text";
			}
		}
	}
}
