﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.Networking;

namespace Goblinsama
{
	public class InstancedElements : MonoBehaviour
	{
		private Dictionary<string,GameObject> folders = new Dictionary<string, GameObject>();
		
		private GameObject _SpawnObject (GameObject prefab, Vector3 position, Quaternion rotation, string folder, bool network)
		{
			GameObject spawned = (GameObject)Instantiate( prefab, position, rotation );
			
			if (folder==null || folder.Length<1)
			{
				folder="default";
			}
			
			ToFolder(spawned,folder);
			
			if (network)
			{
				NetworkServer.Spawn(spawned);
			}
			
			return spawned;
		}
		private T _SpawnObject<T> (T prefab, Vector3 position, Quaternion rotation, string folder, bool network) where T:MonoBehaviour
		{
			return _SpawnObject(prefab.gameObject, position, rotation, folder, network).GetComponent<T>();
		}
		private T _SpawnObject<T> (T prefab, Transform barret, string folder, bool network) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,barret.position,barret.rotation,folder,network);
		}
		
		public T SpawnObject<T> (T prefab) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,Vector3.zero,Quaternion.identity,null,false);
		}
		public T SpawnObject<T> (T prefab, string folder) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,Vector3.zero,Quaternion.identity,folder,false);
		}
		public T SpawnObject<T> (T prefab, Transform barret) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,barret,null,false);
		}
		public T SpawnObject<T> (T prefab, Transform barret, string folder) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,barret,folder,false);
		}
		public T SpawnObject<T> (T prefab, Vector3 position) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,position,Quaternion.identity,null,false);
		}
		public T SpawnObject<T> (T prefab, Vector3 position, string folder) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,position,Quaternion.identity,folder,false);
		}
		public T SpawnObject<T> (T prefab, Vector3 position, Quaternion rotation) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,position,rotation,null,false);
		}
		public T SpawnObject<T> (T prefab, Vector3 position, Quaternion rotation, string folder) where T:MonoBehaviour
		{
			return _SpawnObject(prefab,position,rotation,folder,false);
		}
		
		public T SpawnNetObject<T> (T prefab) where T:NetworkBehaviour
		{
			return _SpawnObject<T>(prefab,Vector3.zero,Quaternion.identity,null,true);
		}
		public T SpawnNetObject<T> (T prefab, string folder) where T:NetworkBehaviour
		{
			return _SpawnObject<T>(prefab,Vector3.zero,Quaternion.identity,folder,true);
		}
		public T SpawnNetObject<T> (T prefab, Vector3 position, string folder) where T:NetworkBehaviour
		{
			return _SpawnObject<T>(prefab,position,Quaternion.identity,folder,true);
		}
		public T SpawnNetObject<T> (T prefab, Vector3 position, Quaternion rotation, string folder) where T:NetworkBehaviour
		{
			return _SpawnObject<T>(prefab,position,rotation,folder,true);
		}
		
		public GameObject SpawnObject (GameObject prefab, Vector3 position, string folder)
		{
			return _SpawnObject(prefab, position, Quaternion.identity, folder, false);
		}
		
		public void ToFolder (GameObject go, string folder)
		{
			if (!folders.ContainsKey(folder))
			{
				// crea la folder in caso questa mancasse
				folders[folder] = new GameObject();
				folders[folder].name = folder;
				folders[folder].transform.SetParent(transform,false);
			}
			
			go.transform.SetParent(folders[folder].transform,true);
		}
	}
}
