import os
import time

MAIN = '../'
ASSET = MAIN+'Assets/'
BUILD = MAIN+'Build/'
SCRIPT = MAIN+'Scripts/'
RELEASE = BUILD+'Release/'
VERSIONS = BUILD+'versions/'
SOURCES = ASSET+'sources/'
'''
HOST = 'rlieh'
'''
OSES = ('osx','win','win32','linux')

from config_local import SCENEFILE

def speak (message, do_print=False):
	if do_print:
		print(message)
	os.system( 'say -v "Fiona" "'+message+'"' )
	os.system( 'terminal-notifier -message "'+message+'" -execute "open -a Terminal"' )
def get_version ():
	return os.popen( "grep version\: %s |cut -d: -f2|cut -d\  -f2" % SCENEFILE ).read().rstrip()
def get_name ():
	return os.popen( "grep 'GAME_NAME' %s |cut -d\\: -f2" % SCENEFILE ).read().strip()
def calc_basedir (base_b):
	return {
		'osx': base_b+'OSX/',
		'win': base_b+'Windows/',
		'win32': base_b+'Windows32/',
		'linux': base_b+'Linux/',
		'server': base_b+'Server/',
		'android': base_b+'Android/',
	}

class ElTime:
	def __init__ (self):
		self.time_start = time.time()
	
	def end (self, rounding=1):
		self.time_end = time.time()
		self.time_elapsed = self.time_end-self.time_start
		if (rounding>=0):
			return round(self.time_elapsed,rounding)
		return self.time_elapsed

NAME = get_name()
lowNAME = NAME.lower().replace(" ","-")
