#!/usr/bin/env python3

import os
import config
import shutil
import sys
from config import ElTime

time_whole = ElTime()

### costanti

'''
TARGET = 'www/pixp.eu/Release/'
'''

### input

if len(sys.argv)>1:
	VERSION = sys.argv[1]
else:
	VERSION = config.get_version()

### variabili interne

base = config.calc_basedir(config.RELEASE+VERSION+'/')

name_b = config.NAME+'-'+VERSION
zip_name = dict()
for osn in config.OSES:
	zip_name[osn] = base[osn]+name_b+'-'+osn # NOTA: non include il .zip, perché make_archive lo aggiunge

dir_source = {
	'osx': base['osx']+'zipdir/',
	'win': base['win']+'zipdir/',
	'win32': base['win32']+'zipdir/',
	'linux': base['linux']+'zipdir/',
	'android': base['android']+'zipdir/',
}

'''
android_source = dir_source['android']+config.NAME+'.apk'
android_target = base['android']+name_b+'.apk'

last_version_file = config.RELEASE+'last_version'
'''

### prima crea gli zip

time_zip = ElTime()

## OS zip

for osn in config.OSES:
	print('creating [%s] from [%s]' % (zip_name[osn],dir_source[osn]))
	shutil.make_archive(zip_name[osn], 'zip', dir_source[osn])

## Android apk

'''
print('copying [%s] from [%s]' % (android_target,android_source))
shutil.copy2( android_source, android_target )
'''

##

print('zips created in '+str(time_zip.end())+' seconds')

'''
### crea un file col numero di versione

with open(last_version_file, 'w') as fi:
	fi.write(VERSION)
fi.close()

### poi li carica sul server

time_upload = ElTime()

files = ""
for osn in config.OSES:
	files = files + ' "' +zip_name[osn]+ '.zip" '
files = files + ' "'+android_target+'" '
files = files + ' "'+last_version_file+'" '

os.system( 'time scp -rC %s "%s:%s"' % (files,config.HOST,TARGET) )

print('zips uploaded in '+str(time_upload.end())+' seconds')
'''

### 

config.speak(config.NAME+' done in '+str(time_whole.end())+' seconds',True)
