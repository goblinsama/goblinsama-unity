#!/bin/bash

printf "" >.gitignore
printf "# File auto-generated, using Goblinsama-Unity/Scripts/create-ignore.sh\n\n" >>.gitignore
printf "/create-ignore.sh\n\n" >>.gitignore
printf "#### #### .gitignore-unity #### ####\n\n" >>.gitignore
cat ~/.gitignore-unity >>.gitignore
printf "\n#### #### .gitignore-local #### ####\n\n" >>.gitignore
cat .gitignore-local >>.gitignore

find . -type l -maxdepth 1 -name create-ignore.sh -delete
find . -type f -name create-ignore.sh -exec ln -s {} \;
