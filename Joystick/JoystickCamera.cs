﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama
{
	[RequireComponent(typeof(Camera))]
	public class JoystickCamera : MonoBehaviour
	{
		private Camera _camera;

		#pragma warning disable 0109
		// NOTA: è pazzo: se non metti new si lamenta che stai nascondendo un elemento e ti invita a metterlo, se lo metti ti dice che non c'era nulla da nascondere e ti invita a toglierlo
		new public Camera camera {
			get {
				if (!_camera)
					_camera = GetComponent<Camera>();
				if (!_camera)
					throw new MissingComponentException(this+": couldn't find a Camera.");
				return _camera;
			}
		}
		#pragma warning restore 0109
	}
}
