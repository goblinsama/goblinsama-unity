﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama.Joystick
{
	public class Stick : Element
	{
		/// <summary>
		/// Permette movimenti orizzontali.
		/// </summary>
		public bool axisH=true;

		/// <summary>
		/// Permette movimenti verticali.
		/// </summary>
		public bool axisV=true;

		public Transform inner;

		protected override bool Update ()
		{
			if (base.Update())
				return true;

			if (isTouching)
			{
				currentTouch=touchDiff;
				if (currentTouch.magnitude>areaRadius)
					currentTouch*=(areaRadius/currentTouch.magnitude);
				
				if (!axisH)
					currentTouch.x = 0;
				if (!axisV)
					currentTouch.y = 0;

				Vector3 vec3 = joystickCamera.camera.ScreenToWorldPoint(areaBounds.center + (Vector3)currentTouch);

				vec3.z = 0;

				inner.position = vec3;
			}

			return false;
		}

		public float GetAxis (string axisName)
		{
			if (axisName=="Horizontal")
				return GetAxisH();
			if (axisName=="Vertical")
				return GetAxisV();
			throw new System.ArgumentException("Unknown axis <"+axisName+">");
		}
		public float GetAxisH ()
		{
			if (!isTouching || !axisH)
				return 0;

			return currentTouch.x/areaRadius;
		}
		public float GetAxisV ()
		{
			if (!isTouching || !axisV)
				return 0;

			return currentTouch.y/areaRadius;
		}

		protected override bool KeepTouching ()
		{
			return true;
		}
		protected override void EndTouch ()
		{
			base.EndTouch();
			inner.localPosition=Vector2.zero;
		}
	}
}
