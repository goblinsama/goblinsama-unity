﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama.Joystick
{
	public abstract class Element : MonoBehaviour
	{
		public enum AnchorH
		{
			Left,
			Right,
		}
		public enum AnchorV
		{
			Bottom,
			Top,
		}
		public AnchorH anchorH = AnchorH.Left;
		public AnchorV anchorV = AnchorV.Bottom;

		/// <summary>
		/// Quanto margine lascia tra l'area e i bordi dello schermo.
		/// </summary>
		public float margin;

		//// elementi trovati a runtime
		
		protected JoystickCamera joystickCamera;
		private CircleCollider2D area;
		
		//// variabili
		
		protected Bounds areaBounds;
		protected float areaRadius;
		
		/// <summary>
		/// True se c'è un tocco valido in corso.
		/// </summary>
		protected bool isTouching;
		
		/// <summary>
		/// In caso isTouching sia true, indica la posizione a schermo del tocco.
		/// Se isTouching è a false, il suo valore è indefinito.
		/// </summary>
		protected Vector2 currentTouch;

		protected Vector3 touchDiff;

		protected virtual void Awake ()
		{
			joystickCamera = FindObjectOfType<JoystickCamera>();
			area = GetComponent<CircleCollider2D>();
			RecalcArea();
		}
		protected virtual void OnEnable ()
		{
			UpdatePosition();
		}
		protected virtual bool Update ()
		{
			if (!Input.GetMouseButton(0))
			{
				EndTouch();
				
				//ClientServer.SetLog("none");
				
				return true;
			}
			
			Vector3 touch = Input.mousePosition;
			touch.z = areaBounds.center.z;
			touchDiff = touch-areaBounds.center;
			float dist = touchDiff.magnitude;
			
			if (dist>areaRadius)
			{
				if (!isTouching || !KeepTouching())
				{
					// ha toccato direttamente fuori, dunque non è un tocco che ci interessa
					// oppure KeepTouching è disabilitato, dunque scarta i tocchi fuori anche se son partiti da dentro

					//ClientServer.SetLog("out");

					return true;
				}
			}
			
			// ha toccato dentro, oppure ora è fuori ma inizialmente era partito da dentro (dunque continuiamo a tracciarlo)

			//ClientServer.SetLog("in");
			
			isTouching=true;

			return false;
		}
		
		private void UpdatePosition ()
		{
			float marginPixel = areaRadius + margin;
			
			Vector2 posPixel;
			posPixel.x = anchorH==AnchorH.Left ? marginPixel : Screen.width-marginPixel;
			posPixel.y = anchorV==AnchorV.Bottom ? marginPixel : Screen.height-marginPixel;

			Vector2 posScreen = joystickCamera.camera.ScreenToWorldPoint(posPixel);
			transform.position = posScreen;
			
			RecalcArea();
		}
		private void RecalcArea ()
		{
			Vector2 areaMin = joystickCamera.camera.WorldToScreenPoint(area.bounds.min);
			Vector2 areaMax = joystickCamera.camera.WorldToScreenPoint(area.bounds.max);
			areaBounds = new Bounds();
			areaBounds.SetMinMax(areaMin,areaMax);
			areaRadius = areaBounds.max.x-areaBounds.center.x;
		}

		protected abstract bool KeepTouching ();

		protected virtual void EndTouch ()
		{
			isTouching=false;
		}
	}
}
