﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2015-2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;

namespace Goblinsama
{
	[RequireComponent(typeof(SpriteRenderer))]
	public class SpriteTile : MonoBehaviour
	{
		[SerializeField] private GameObject topAngle;
		
		// qui salvo quante celle ci saranno, dipendono dalla size dell'oggetto che conterrà gli N x M sprite
		private int gridX = 1;
		private int gridY = 1;
		
		private SpriteRenderer sprite;
		
		void Awake ()
		{
			sprite = GetComponent<SpriteRenderer>();
			
			// il numero di righe e colonne di sprite di dimensione unitaria che vogliamo (la local scale DEVE quindi essere un numero intero, altrimenti ci saranno degli spazi vuoti)
			gridX = (int)transform.localScale.x;
			gridY = (int)transform.localScale.y;
			
			// filla e tila la griglia con x colonne e y righe
			Tile();
		}
		
		void AngleTop (float x, float y)
		{
			if (topAngle == null)
				return;
			
			// prende dimensioni e scala dello sprite originale
			SpriteRenderer angleSprite = topAngle.GetComponent<SpriteRenderer>();
			Vector2 spriteExtents = new Vector2(angleSprite.bounds.extents.x, angleSprite.bounds.extents.y);
			
			float adjY = y + spriteExtents.y;
			
			topAngle.transform.position = transform.position + (new Vector3(x, adjY, 0f));
			topAngle.transform.parent = transform;			
		}
		
		/// <summary>
		/// Fa il tile dello sprite, in base alla size. Riempie una griglia di x righe e y colonne.
		/// Se c'è un solo sprite, o ripetizioni dispari, lo sprite al centro avrà posizione 0,0 rispetto al contenitore.
		/// Altrimenti vengono splittati di mezzo sprite a sinistra ed in basso, per fare in modo che gli angoli degli sprite al centro siano in 0,0 rispetto al contenitore.
		/// </summary>
		void Tile ()
		{
			// prende dimensioni e scala dello sprite originale
			Vector2 spriteExtents = new Vector2(sprite.bounds.extents.x, sprite.bounds.extents.y);
			
			// crea il prefab dello sprite
			GameObject childPrefab = new GameObject();
			SpriteRenderer childSprite = childPrefab.AddComponent<SpriteRenderer>();
			childPrefab.transform.position = transform.position;
			childSprite.sprite = sprite.sprite;
			childSprite.sortingOrder = sprite.sortingOrder;
			childSprite.flipX = sprite.flipX;
			childSprite.flipY = sprite.flipY;
			childSprite.sortingLayerID = sprite.sortingLayerID;
			
			// figlio temporaneo per instanziare i vari cloni
			GameObject child;
			
			// aggiusto il pivot di partenza nel caso di una griglia con elementi pari
			float pivotX = 0f, pivotY = 0f;
			if (gridX % 2 == 0)
			{
				pivotX = spriteExtents.x / transform.localScale.x;
			}
			if (gridY % 2 == 0)
			{
				pivotY = spriteExtents.y / transform.localScale.y;
			}
			
			// calcolo il punto di partenza per il tile
			float startX, startY;
			startX = -gridX / 2;
			startY = -gridY / 2;
			
			// calcolo il fattore di scala
			Vector3 spriteScale = Vector3.zero;
			spriteScale.x = spriteExtents.x * 2 / transform.localScale.x;
			spriteScale.y = spriteExtents.y * 2 / transform.localScale.y;
			
			// riempio la griglia
			for (int cc = 0; cc < gridX; cc++)
			{
				for (int rr = 0; rr < gridY; rr++)
				{
					child = Instantiate(childPrefab) as GameObject;
					child.name = transform.name + " (Clone)" + "R: " + rr + "; C:" + cc;
					float x = pivotX + spriteScale.x * (startX + cc);
					float y = pivotY + spriteScale.y * (startY + rr);
					child.transform.position = transform.position + (new Vector3(x, -y, 0f));
					child.transform.parent = transform;
					
					if ( rr == 0 && cc == 0 )
					{
						float adjY = -y + spriteScale.y / 2;
						AngleTop(x, adjY);
					}
				}
			}
			
			// distruggo il prefab del figlio generico
			Destroy(childPrefab);
			
			// disabilito lo sprite renderer del padre, lasciando ai cloni (figli) il compito di renderarsi
			sprite.enabled = false;
		}
	}
}
