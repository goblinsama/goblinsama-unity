﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2010,2013-2017 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace Goblinsama.SpawnPoints
{
	public class SpawnPoints : MonoBehaviour
	{
		public enum GizmoType {
			Cube,
			Sphere,
		}
		
		public bool random;
		public Color gizmoColor = new Color32(166,241,201,255);
		public GizmoType gizmoType = GizmoType.Cube;
		
		/// <summary>
		/// Gli SpawnPoint al momento attivi.
		/// </summary>
		protected List<SpawnPoint> _pointsActive;
		protected List<SpawnPoint> pointsActive {
			get {
				if (_pointsActive==null)
					_pointsActive = new List<SpawnPoint>(GetComponentsInChildren<SpawnPoint>());
				return _pointsActive;
			}
		}
		
		public int Count {
			get {
				return pointsActive.Count;
			}
		}
		
		private int _pointCur;
		protected int pointCur {
			get {
				return _pointCur;
			}
			set {
				if (value >= pointsActive.Count)
				{
					_pointCur = 0;
				}
				else if (value < 0)
				{
					_pointCur = pointsActive.Count-1;
				}
				else
				{
					_pointCur = value;
				}
			}
		}
		
		protected virtual void Start ()
		{
			#pragma warning disable 0219
			int cou = pointsActive.Count; // NOTA: serve a inizializzare pointsActive
			#pragma warning restore 0219
		}
		
		public virtual SpawnPoint GetNextSpawnLocation ()
		{
			SpawnPoint point;
			
			if (random)
			{
				point = pointsActive[Random.Range(0,pointsActive.Count)];
			}
			else
			{
				point = pointsActive[pointCur++];
			}
			
			return point;
		}
		public bool IsActive (SpawnPoint point)
		{
			return (point!=null) && pointsActive.Contains(point);
		}
	}
}
