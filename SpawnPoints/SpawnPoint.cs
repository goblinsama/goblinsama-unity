﻿/*	Goblinsama-Unity Library
	https://bitbucket.org/goblinsama/goblinsama-unity
	
	© 2010,2013-2016 Goblinsama Ltd. <goblinsama+src@goblinsama.com> https://goblinsama.com
	
	This source code is subject to the terms of the Microsoft Public License (MS-PL).
	
	Redistribution and use in source and binary forms, with or without modification,
	is permitted provided that redistributions of the source code retain the above
	copyright notices and this file header.
	
	For details, see LICENSE file, or visit http://www.opensource.org/licenses/ms-pl.html
	
	All other rights reserved.
*/

using UnityEngine;
using System.Collections;

namespace Goblinsama.SpawnPoints
{
	public class SpawnPoint : MonoBehaviourE
	{
		// [SerializeField] protected Vector3 initialImpulse;
		
		protected Color gizmoColor;
		
		#if UNITY_EDITOR
		private SpawnPoints _spawnPoints;
		protected SpawnPoints spawnPoints {
			get {
				if (_spawnPoints == null)
					_spawnPoints = GetComponentInParent<SpawnPoints>();
				return _spawnPoints;
			}
		}
		
		protected override void Start ()
		{
			base.Start();
			gizmoColor = spawnPoints.gizmoColor;
		}
		protected override void OnDrawGizmos ()
		{
			base.OnDrawGizmos();
			
			Gizmos.color = gizmoColor;
			if (spawnPoints.gizmoType == SpawnPoints.GizmoType.Cube)
			{
				Gizmos.DrawCube( transform.position, Vector3.one/1.5f );
			}
			else if (spawnPoints.gizmoType == SpawnPoints.GizmoType.Sphere)
			{
				Gizmos.DrawSphere( transform.position, 1f/1.5f );
			}
		}
		#endif
	}
}
